package anhbttester.pages.AdminPage;

import anhbttester.base.ValidateHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ManageArtistPage {
    private WebDriver driver;
    private ValidateHelper validateHelper;
    private By manageArtistMenu = By.xpath("//h6[@class='MuiTypography-root MuiTypography-h6 css-5vcywl'][normalize-space()='Manage artist']");
    private By actionBtn = By.xpath("//body//div[@id='root']//div[@role='presentation']//div[@role='presentation']//div[@role='rowgroup']//div[1]//div[9]//button[1]");
    private By seemoreaccountBtn = By.xpath("//li[normalize-space()='See more account']");
    private By seePostBtn = By.xpath("//li[normalize-space()='See more artist posts']");
    private By statisticBtn = By.xpath("//li[normalize-space()='View statistic artist']");
    private By actionDeactiveBtn = By.cssSelector("li:nth-child(4)");
    private By actionActiveBtn = By.xpath("//li[normalize-space()='Active artist']");
    private By typeSelect = By.xpath("//body/div[@role='presentation']/div[@class='MuiBox-root css-1s9b7fl']/form[@action='#']/div[1]");
    private By reasonInput = By.xpath("//label[@id=':r3o:-label']");
    private By reasonActiveInput = By.xpath("//textarea[@id=':r62:']");
    private By sendDeactiveBtn = By.xpath("//button[normalize-space()='Deactive artist']");

    public ManageArtistPage(WebDriver driver){
        this.driver = driver;
        validateHelper = new ValidateHelper(driver);
    }
    public void seemoreaccount() {
        validateHelper.clickElement(manageArtistMenu);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(actionBtn);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(seemoreaccountBtn);
        validateHelper.waitForPageLoaded();
    }
    public void seemoreartistpost() {
        //validateHelper.clickElement(manageArtistMenu);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(actionBtn);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(seePostBtn);
        validateHelper.waitForPageLoaded();
    }
    public void viewstatistic() {
        //validateHelper.clickElement(manageArtistMenu);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(actionBtn);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(statisticBtn);
        validateHelper.waitForPageLoaded();
    }

    public void deactiveOrActivateUser(String type, String reason) {
        //validateHelper.clickElement(manageArtistMenu);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(actionBtn);
        validateHelper.waitForPageLoaded();
        if (isArtistActive()) {
            active(reason);
        } else {
            deactive(type, reason);
        }
    }

    private boolean isArtistActive() {
        String buttonText = validateHelper.getElementText(actionDeactiveBtn);
        return buttonText.trim().equalsIgnoreCase("Deactive");
    }

    public void deactive(String type, String reason){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(actionDeactiveBtn);
        validateHelper.waitForPageLoaded();
        if (isArtistActive()) {
            validateHelper.clickElementByJs(typeSelect);
            validateHelper.waitForPageLoaded();
            validateHelper.selectOptionInMenu(typeSelect, type);
        }
        validateHelper.waitForPageLoaded();
        validateHelper.setText(reasonInput, reason);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(sendDeactiveBtn);
    }
    public void active(String reason) {

            validateHelper.waitForPageLoaded();
            validateHelper.clickElementByJs(actionDeactiveBtn);
            validateHelper.waitForPageLoaded();
            validateHelper.setText(reasonInput, reason);
            validateHelper.waitForPageLoaded();
            validateHelper.clickElement(sendDeactiveBtn);

//        validateHelper.setText(reasonActiveInput, reason);
//        validateHelper.waitForPageLoaded();
//        validateHelper.clickElement(sendDeactiveBtn);
    }

    //Process check status Artist


}


