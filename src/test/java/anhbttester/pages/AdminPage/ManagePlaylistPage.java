package anhbttester.pages.AdminPage;

import anhbttester.base.ValidateHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

public class ManagePlaylistPage {
    private WebDriver driver;
    private ValidateHelper validateHelper;
    private By managePlaylistMenu = By.xpath("//h6[@class='MuiTypography-root MuiTypography-h6 css-5vcywl'][normalize-space()='Manage playlist']");
    private By actionMoreBtn = By.xpath("(//button[@aria-label='More options for manager'])[1]");
    private By seeMoreCreator = By.xpath("//li[normalize-space()='See more creator']");
    private By seeMoreTracks = By.xpath("//li[normalize-space()='See more tracks']");
    private By undoVerifyPlaylist = By.xpath("//li[normalize-space()='Undo verify playlist']");
    private By confirmundoVerifyPlaylist = By.xpath("//button[normalize-space()='Cancel']");
    private By deletePlaylist = By.xpath("//li[normalize-space()='Delete playlist']");
    public ManagePlaylistPage(WebDriver driver){
        this.driver = driver;
        validateHelper = new ValidateHelper(driver);
    }
    public void seemorecreator(){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(managePlaylistMenu);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(actionMoreBtn);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(seeMoreCreator);
    }
    public void seemoretrack(){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(actionMoreBtn);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(seeMoreTracks);
    }
    public void undoverifyplaylist(){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(actionMoreBtn);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(undoVerifyPlaylist);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(confirmundoVerifyPlaylist);
    }
    public void deleteplaylist() {
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(actionMoreBtn);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(deletePlaylist);
        Actions actions = new Actions(driver);
        actions.moveByOffset(10, 20);
        actions.click().perform();
    }
}
