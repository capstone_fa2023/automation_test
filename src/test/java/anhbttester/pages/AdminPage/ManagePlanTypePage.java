package anhbttester.pages.AdminPage;

import anhbttester.base.ValidateHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

public class ManagePlanTypePage {
    private WebDriver driver;
    private ValidateHelper validateHelper;
    private By managePlanTypeMenu = By.xpath("//h6[normalize-space()='Manage plan type']");
    private By createPlanBaseBtn = By.xpath("//button[normalize-space()='Create plan type']");
    private By nameInput = By.name("planName");
    private By multiplicationInput = By.name("multiplication");
    private By discountInput = By.name("discount");
    private By planBaseSelect = By.id("demo-simple-select-label");
    private By planBaseSelect2 = By.xpath("//body/div[@role='presentation']/div[@class='MuiBox-root css-bq69ut']/div[@id='modal-modal-description']/form[@action='#']/div[@class='MuiBox-root css-0']/div/div[@class='MuiFormControl-root MuiFormControl-fullWidth css-tzsjye']/div[1]//*[name()='svg']");
    private By optionPlanBase = By.xpath("(//li[contains(@role,'option')])[3]");
    private By actionMoreBtn = By.xpath("(//button[@aria-label='More options for manager'])[1]");
    private By hidePlanTypeBtn = By.xpath("//li[normalize-space()='Hide plan type']");
    private By confirmHideBtn = By.xpath("//button[normalize-space()='Cancel']");
    private By viewStatisticBtn = By.xpath("//li[normalize-space()='View statistic']");
    public ManagePlanTypePage(WebDriver driver){
        this.driver = driver;
        validateHelper = new ValidateHelper(driver);
    }
    public void createplantype(String name, String multiplication, String discount){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(managePlanTypeMenu);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(createPlanBaseBtn);
        validateHelper.waitForPageLoaded();
        validateHelper.setText(nameInput, name);
        validateHelper.waitForPageLoaded();
        validateHelper.setText(multiplicationInput, multiplication);
        validateHelper.waitForPageLoaded();
        validateHelper.setText(discountInput, discount);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(planBaseSelect);
        validateHelper.waitForPageLoaded();
        Actions actions = new Actions(driver);
        actions.moveByOffset(10, 22);
        actions.click().perform();
    }
    public void hideplantype(){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(actionMoreBtn);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(hidePlanTypeBtn);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(confirmHideBtn);
    }
    public void statisticplantype(){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(actionMoreBtn);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(viewStatisticBtn);
    }
}
