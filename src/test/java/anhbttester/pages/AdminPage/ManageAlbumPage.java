package anhbttester.pages.AdminPage;

import anhbttester.base.ValidateHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

import javax.swing.*;

public class ManageAlbumPage {
    private WebDriver driver;
    private ValidateHelper validateHelper;
    private By manageAlbumMenu = By.xpath("//h6[@class='MuiTypography-root MuiTypography-h6 css-5vcywl'][normalize-space()='Manage album']");
    private By actionMoreBtn = By.xpath("(//button[@aria-label='More options for manager'])[1]");
    private By seeMoreTracks = By.xpath("//li[normalize-space()='See more tracks']");
    private By seeMoreCreator = By.xpath("//li[normalize-space()='See more creator']");
    private By undoVerifyAlbum = By.xpath("//li[normalize-space()='Undo verify album']");
    private By confirmundoVerifyAlbum = By.xpath("//button[normalize-space()='Cancel']");
    private By deleteAlbum = By.xpath("//li[normalize-space()='Delete album']");
    public ManageAlbumPage(WebDriver driver){
        this.driver = driver;
        validateHelper = new ValidateHelper(driver);
    }
    public void seemoretracks(){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(manageAlbumMenu);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(actionMoreBtn);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(seeMoreTracks);
    }
    public void seemorecreator(){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(actionMoreBtn);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(seeMoreCreator);
    }
    public void undoverifyalbum(){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(actionMoreBtn);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(undoVerifyAlbum);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(confirmundoVerifyAlbum);
    }
    public void deletealbum() {
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(actionMoreBtn);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(deleteAlbum);
        Actions actions = new Actions(driver);
        actions.moveByOffset(10, 20);
        actions.click().perform();
    }
}
