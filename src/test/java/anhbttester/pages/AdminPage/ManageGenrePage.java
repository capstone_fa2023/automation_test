package anhbttester.pages.AdminPage;

import anhbttester.base.ValidateHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ManageGenrePage {
    private WebDriver driver;
    private ValidateHelper validateHelper;
    private By manageGenreMenu = By.xpath("//h6[normalize-space()='Manage genre']");
    private By createGenreBtn = By.xpath("//button[normalize-space()='Create Genre']");
    private By actionMoreBtn = By.xpath("//body//div[@id='root']//div[@role='presentation']//div[@role='presentation']//div[@role='rowgroup']//div[1]//div[4]//div[1]//button[1]");
    private By editGenreBtn = By.xpath("//li[normalize-space()='Edit genre']");
    private By updateGenreBtn = By.xpath("//button[normalize-space()='Update']");
    private By hideGenreBtn = By.xpath("//li[normalize-space()='Hide genre']");
    private By confirmHideBtn = By.xpath("//button[normalize-space()='Cancel']");
    public ManageGenrePage(WebDriver driver){
        this.driver = driver;
        validateHelper = new ValidateHelper(driver);
    }
    public void creategenre(){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(manageGenreMenu);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(createGenreBtn);
    }
    public void editgenre(){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(actionMoreBtn);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(editGenreBtn);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(updateGenreBtn);
    }
    public void hidegenre(){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(actionMoreBtn);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(hideGenreBtn);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(confirmHideBtn);
    }
}
