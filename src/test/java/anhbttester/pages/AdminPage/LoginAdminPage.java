package anhbttester.pages.AdminPage;

import anhbttester.base.ValidateHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

public class LoginAdminPage {
    private WebDriver driver;
    private ValidateHelper validateHelper;
    private String url = "/m/login";
    private By emailInput = By.xpath("//input[@id='login-username']");
    private By passwordInput = By.xpath("//input[@id='login-password']");
    private By loginBtn = By.xpath("//button[@type='submit']");
    public LoginAdminPage(WebDriver driver)
    {
        this.driver = driver;
        validateHelper = new ValidateHelper(driver);
    }
    public void loginAdmin(String emailValue, String passwordValue)
    {
        validateHelper.waitForPageLoaded();
        Assert.assertTrue(validateHelper.verifyUrl(url), "It's not Login Page");
        validateHelper.waitForPageLoaded();
        validateHelper.setText(emailInput,emailValue);
        validateHelper.setText(passwordInput,passwordValue);
        validateHelper.clickElement(loginBtn);
    }
}
