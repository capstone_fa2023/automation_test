package anhbttester.pages.AdminPage;

import anhbttester.base.ValidateHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ManagePlanTransactionPage {
    private WebDriver driver;
    private ValidateHelper validateHelper;
    private By managePlanTransactionMenu = By.xpath("//h6[@class='MuiTypography-root MuiTypography-h6 css-5vcywl'][normalize-space()='Manage plan transaction']");
    private By actionMoreBtn = By.xpath("(//button[@aria-label='More options for manager'])[1]");
    private By seeMorePlanBtn = By.xpath("//li[normalize-space()='See more plan']");
    private By seeMoreCreatorBtn = By.xpath("//li[normalize-space()='See more creator']");
    public ManagePlanTransactionPage(WebDriver driver){
        this.driver = driver;
        validateHelper = new ValidateHelper(driver);
    }
    public void seemoreplan(){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(managePlanTransactionMenu);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(actionMoreBtn);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(seeMorePlanBtn);
    }
    public void seemorecreator(){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(actionMoreBtn);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(seeMoreCreatorBtn);
    }
}
