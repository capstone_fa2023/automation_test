package anhbttester.pages.AdminPage;

import anhbttester.base.ValidateHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class ManageUserPage {
    private WebDriver driver;
    private ValidateHelper validateHelper;
    private By deactiveBtn = By.xpath("//*[@id=\"root\"]/div/main/div[3]/div/div/div/div[1]/div[2]/div/div/div[1]/div[7]/button");
    private By typeSelect = By.xpath("//div[@id='demo-simple-select']");
    private By reasonInput = By.xpath("//textarea[@name='reason']");
    private By sendDeactiveBtn = By.xpath("//button[@type='submit']");
    private By manageUserMenu = By.xpath("//h6[@class='MuiTypography-root MuiTypography-h6 css-5vcywl'][normalize-space()='Manage customer']");
    public ManageUserPage(WebDriver driver){
        this.driver = driver;
        validateHelper = new ValidateHelper(driver);
    }

    public void deactiveOrActivateUser(String type, String reason) {
        validateHelper.clickElement(manageUserMenu);
        validateHelper.waitForPageLoaded();
        if (isUserActive()) {
            deactive(type, reason);
        } else {
            active(reason);
        }
    }

    private boolean isUserActive() {
        String buttonText = validateHelper.getElementText(deactiveBtn);
        return buttonText.trim().equalsIgnoreCase("Deactive");
    }

    public void deactive(String type, String reason){
        validateHelper.clickElement(deactiveBtn);
        validateHelper.waitForPageLoaded();
        // Click type only if the button text is "Deactivate"
        if (isUserActive()) {
            // Click type
            validateHelper.clickElementByJs(typeSelect);

            // Option type
            validateHelper.waitForPageLoaded();
            validateHelper.selectOptionInMenu(typeSelect, type);
        }
        //itemToClick.click();
        //input reason
        validateHelper.setText(reasonInput, reason);
        validateHelper.clickElement(sendDeactiveBtn);
    }
    public void active(String reason){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(deactiveBtn);
        validateHelper.waitForPageLoaded();
        //input reason
        validateHelper.setText(reasonInput, reason);
        validateHelper.clickElement(sendDeactiveBtn);
    }
}
