package anhbttester.pages.AdminPage;

import anhbttester.base.ValidateHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ManageModPage {
    private WebDriver driver;
    private ValidateHelper validateHelper;
    private By manageModMenu = By.xpath("//h6[normalize-space()='Manage mod']");
    private By createMod = By.xpath("//button[normalize-space()='Create mod account']");
    //private By emailInput = By.xpath("//div[@class='MuiInputBase-root MuiOutlinedInput-root MuiInputBase-colorPrimary Mui-error MuiInputBase-formControl css-x890mn']//input[@name='email']");
    private By createBtn = By.xpath("//button[normalize-space()='Create']");
    private By changeAuthorityBtn = By.xpath("//button[contains(text(), 'Change authority')]");
    private By viewauthorities = By.xpath("//button[@aria-label='See more authorities']");
    public ManageModPage(WebDriver driver){
        this.driver = driver;
        validateHelper = new ValidateHelper(driver);
    }
    public void createmod(String email){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(manageModMenu);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(createMod);
        validateHelper.waitForPageLoaded();
        WebElement modal = driver.findElement(By.id("modal-modal-description"));
        // Xác định phần tử input email trong modal
        WebElement emailInput = modal.findElement(By.name("email"));
        emailInput.sendKeys(email);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(createBtn);
    }
    public void changeauthority(String checkboxValue){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(changeAuthorityBtn);
        validateHelper.waitForPageLoaded();
        WebElement modal = driver.findElement(By.id("modal-modal-description"));
        WebElement checkbox = modal.findElement(By.xpath("//span[text()='" + checkboxValue + "']/preceding-sibling::span/input"));
        // Kiểm tra checkbox có được chọn hay không
        if (!checkbox.isSelected()) {
            checkbox.click();
        }
        WebElement updateButton = modal.findElement(By.xpath("//button[contains(text(), 'Update')]"));
        updateButton.click();
    }
    public void authorities(){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(viewauthorities);
    }
}
