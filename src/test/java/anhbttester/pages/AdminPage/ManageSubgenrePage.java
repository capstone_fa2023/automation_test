package anhbttester.pages.AdminPage;

import anhbttester.base.ValidateHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class ManageSubgenrePage {
    private WebDriver driver;
    private ValidateHelper validateHelper;
    private By manageSubenreMenu = By.xpath("//h6[normalize-space()='Manage sub genre']");
    private By createSubgenreBtn = By.xpath("//button[normalize-space()='Create Sub genre']");
    private By nameInput = By.name("name");
    private By selectGenre = By.id("demo-simple-select");
    private By optionGenre = By.xpath("//li[contains(text(), 'Bolero')]");
    private By createBtn = By.xpath("//li[normalize-space()='Bolero']");
    private By actionMoreBtn = By.xpath("(//button[@aria-label='More options for manager'])[1]");
    private By editSubgenreBtn = By.xpath("//li[normalize-space()='Edit sub genre']");
    private By updateSubgenreBtn = By.xpath("//button[normalize-space()='Update']");
    private By hideSubgenreBtn = By.xpath("//li[normalize-space()='Hide sub genre']");
    private By confirmHideBtn = By.xpath("//button[normalize-space()='Cancel']");
    public ManageSubgenrePage(WebDriver driver){
        this.driver = driver;
        validateHelper = new ValidateHelper(driver);
    }
    public void createsubgenre(String name){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(manageSubenreMenu);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(createSubgenreBtn);
        validateHelper.setText(nameInput, name);
        Actions actions = new Actions(driver);
        actions.moveByOffset(10, 20);
        actions.click().perform();
    }
    public void editsubgenre(){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(actionMoreBtn);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(editSubgenreBtn);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(updateSubgenreBtn);
    }
    public void hidesubgenre(){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(actionMoreBtn);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(hideSubgenreBtn);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(confirmHideBtn);
    }
}
