package anhbttester.pages.AdminPage;

import anhbttester.base.ValidateHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

public class ManageReportPage {
    private WebDriver driver;
    private ValidateHelper validateHelper;
    private By manageReportMenu = By.xpath("//h6[contains(@class,'MuiTypography-root MuiTypography-h6 css-5vcywl')][normalize-space()='Manage report']");
    private By actionMoreBtn = By.xpath("(//button[@aria-label='More options for manager'])[1]");
    private By seeMoreReport = By.xpath("//li[normalize-space()='See more reports']");
    private By deactiveReport = By.xpath("//li[normalize-space()='Deactive']");
    public ManageReportPage(WebDriver driver){
        this.driver = driver;
        validateHelper = new ValidateHelper(driver);
    }
    public void seemorereport(){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(manageReportMenu);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(actionMoreBtn);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(seeMoreReport);
    }
    public void deactivereport(){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(actionMoreBtn);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(deactiveReport);
        Actions actions = new Actions(driver);
        actions.moveByOffset(10, 20);
        actions.click().perform();
    }
}
