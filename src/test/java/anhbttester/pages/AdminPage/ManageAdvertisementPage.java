package anhbttester.pages.AdminPage;

import anhbttester.base.ValidateHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ManageAdvertisementPage {
    private WebDriver driver;
    private ValidateHelper validateHelper;
    private By manageAdvertisementMenu = By.xpath("//h6[@class='MuiTypography-root MuiTypography-h6 css-5vcywl'][normalize-space()='Manage advertisement']");
    private By actionMoreBtn = By.xpath("(//button[@aria-label='More options for manager'])[1]");
    private By seeMoreAttachment = By.xpath("//li[normalize-space()='See more attachments']");
    private By editAdvertisement = By.xpath("//li[normalize-space()='Edit advertisement']");
    private By updateAdvertisementBtn = By.xpath("//button[normalize-space()='Update']");
    private By deleteAdvertisementBtn = By.xpath("//li[normalize-space()='Delete advertisement']");
    private By cfdeleteAdvertisementBtn = By.xpath("//button[normalize-space()='Cancel']");
    public ManageAdvertisementPage(WebDriver driver){
        this.driver = driver;
        validateHelper = new ValidateHelper(driver);
    }
    public void seemoreattachment(){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(manageAdvertisementMenu);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(actionMoreBtn);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(seeMoreAttachment);
    }
    public void editadvertisement(){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(actionMoreBtn);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(editAdvertisement);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(updateAdvertisementBtn);
    }
    public void deletedvertisement(){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(actionMoreBtn);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(deleteAdvertisementBtn);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(cfdeleteAdvertisementBtn);
    }
}
