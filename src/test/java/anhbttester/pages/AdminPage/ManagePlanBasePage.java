package anhbttester.pages.AdminPage;

import anhbttester.base.ValidateHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ManagePlanBasePage {
    private WebDriver driver;
    private ValidateHelper validateHelper;
    private By managePlanBaseMenu = By.xpath("//h6[normalize-space()='Manage plan base']");
    private By createPlanBaseBtn = By.xpath("//button[normalize-space()='Create plan base']");
    private By nameInput = By.name("name");
    private By priceInput = By.name("price");
    private By durationMonthInput = By.name("durationMonth");
    private By createBtn = By.xpath("//button[normalize-space()='Create']");
    private By actionMoreBtn = By.xpath("(//button[@aria-label='More options for manager'])[1]");
    private By hidePlanBaseBtn = By.xpath("//li[normalize-space()='Hide plan base']");
    private By confirmBtn = By.xpath("//button[normalize-space()='Cancel']");
    private By viewStatistic = By.xpath("//li[normalize-space()='View statistic']");
    public ManagePlanBasePage(WebDriver driver){
        this.driver = driver;
        validateHelper = new ValidateHelper(driver);
    }
    public void createplanbase(String name, String price, String duration_month){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(managePlanBaseMenu);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(createPlanBaseBtn);
        validateHelper.waitForPageLoaded();
        validateHelper.setText(nameInput, name);
        validateHelper.waitForPageLoaded();
        validateHelper.setText(priceInput, price);
        validateHelper.waitForPageLoaded();
        validateHelper.setText(durationMonthInput, duration_month);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(createBtn);
    }
    public void hideplanbase(){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(actionMoreBtn);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(hidePlanBaseBtn);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(confirmBtn);
    }
    public void viewstatisticplanbase(){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(actionMoreBtn);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(viewStatistic);
    }
}
