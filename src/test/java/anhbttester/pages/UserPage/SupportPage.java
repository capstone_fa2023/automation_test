package anhbttester.pages.UserPage;

import anhbttester.base.ValidateHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SupportPage {
    private WebDriver driver;
    private ValidateHelper validateHelper;
    private By avtImge = By.xpath("//img[@class='rounded-circle']");
    private By forSupport = By.xpath("//span[contains(text(),'Hỗ trợ')]");
    private By homepage = By.xpath("//a[@class='breadcrumb-item']");
    private By forPassword = By.xpath("//a[contains(text(),'Không thể đặt lại mật khẩu')]");
    private By forgotInfor = By.xpath("//a[contains(text(),'Bạn không nhớ thông tin đăng nhập')]");
    private By supportLoginFb = By.xpath("//a[contains(text(),'Trợ giúp về đăng nhập bằng Facebook')]");
    private By supportLoginGb = By.xpath("//a[contains(text(),'Trợ giúp về đăng nhập bằng Google')]");
    private By supportDisable = By.xpath("//a[contains(text(),'Tài khoản bị vô hiệu hóa')]");
    public SupportPage (WebDriver driver){
        this.driver = driver;
        validateHelper = new ValidateHelper(driver);
    }
    public void opensupportpage(){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(avtImge);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(forSupport);
    }
    public void opensupportforpassword(){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(forPassword);
    }
    public void opensupportforinfor(){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(homepage);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(forgotInfor);
    }
    public void opensupportforloginfb(){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(homepage);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(supportLoginFb);
    }
    public void opensupportforlogingg(){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(homepage);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(supportLoginGb);
    }
    public void opensupportfordisable(){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(homepage);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(supportDisable);
    }
}
