package anhbttester.pages.UserPage;

import anhbttester.base.ValidateHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class PremiumPage {
    private WebDriver driver;
    private ValidateHelper validateHelper;
    private By premiumBtn = By.xpath("//span[normalize-space()='Khám phá']");
    private By startplantypeBtn = By.xpath("//div[@id='premium-page']//div[1]//div[1]//div[1]//a[1]//span[1]");
    private By dayPayment = By.xpath("//body/div[@id='root']/div[@class='App']/div[@id='premium-page']/div[@class='premium-page-content']/main[@id='plan-base']/section/article[@class='collapsable-plans']/ul[@class='list-unstyled']/li[1]/div[1]");
    private By paymentMethod = By.xpath("//ul[@id='plan-type-0']//li[1]//a[1]");
    private By cardNumberInput = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/form[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]/input[1]");
    private By expirationInput = By.xpath("//input[@id='Field-expiryInput']");
    private By cvcInput = By.xpath("//input[@id='Field-cvcInput']");
    private By countrySelect = By.xpath("//select[@id='Field-countryInput']");
    private By payNowBtn = By.xpath("//span[@class='indicator-label']");
    public PremiumPage(WebDriver driver){
        this.driver = driver;
        validateHelper = new ValidateHelper(driver);
    }
    public void openpremiumpage(){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(premiumBtn);
    }
    public void plantype(){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(startplantypeBtn);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(dayPayment);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(paymentMethod);
    }
    public void inforcard(String card_number,String expiration, String cvc, String country){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(cardNumberInput);
        validateHelper.setText(cardNumberInput, card_number);
        validateHelper.waitForPageLoaded();
        validateHelper.setText(expirationInput, expiration);
        validateHelper.waitForPageLoaded();
        validateHelper.setText(cvcInput, cvc);
        validateHelper.waitForPageLoaded();
        validateHelper.selectOptionByText(countrySelect, country);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(payNowBtn);
    }
}
