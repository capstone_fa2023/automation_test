package anhbttester.pages.UserPage;

import anhbttester.base.ValidateHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

public class LoginPage {

    private WebDriver driver;
    private ValidateHelper validateHelper;
    private String url = "/auth/login";
    private By emailInput = By.xpath("//input[@id='login-username']");
    private By passwordInput = By.xpath("//input[@id='login-password']");
    private By loginBtn = By.xpath("//button[@type='submit']");

    public LoginPage(WebDriver driver)
    {
        this.driver = driver;
        validateHelper = new ValidateHelper(driver);
    }
    public void login(String emailValue, String passwordValue)
    {
        Assert.assertTrue(validateHelper.verifyUrl(url), "It's not Login Page");
        validateHelper.waitForPageLoaded();
        validateHelper.setText(emailInput,emailValue);
        validateHelper.setText(passwordInput,passwordValue);
        validateHelper.clickElement(loginBtn);
    }

}
