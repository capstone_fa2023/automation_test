package anhbttester.pages.UserPage;

import anhbttester.base.ValidateHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SignupPage {
    private WebDriver driver;
    private ValidateHelper validateHelper;
    private By emailInput = By.xpath("//input[@id='email']");
    private By firstnameInput = By.xpath("//input[@id='fname']");
    private By lastnameInput = By.xpath("//input[@id='lname']");
    private By passwordInput = By.xpath("//input[@id='password']");
    private By confirmPassInput = By.xpath("//input[@id='re-password']");
    private By dateInput = By.xpath("//input[@id='day']");
    private By monthInput = By.xpath("//select[@id='month']");
    private By yearInput = By.xpath("//input[@id='year']");
    private By maleRadioBtn = By.xpath("//input[@id='gender_option_male']");
    private By femaleRadioBtn = By.xpath("//input[@id='gender_option_female']");
    private By checkbox1 = By.xpath("//input[@id='marketing-opt-checkbox']");
    private By checkbox2 = By.xpath("//input[@id='third-party-checkbox']");
    private By signupBtn = By.xpath("//button[@type='submit']");

    public SignupPage(WebDriver driver)
    {
        this.driver = driver;
        validateHelper = new ValidateHelper(driver);
    }
    public void signup(String emailValue, String fname, String lname,String password, String cfpass, String day, String month, String year)
    {
        validateHelper.waitForPageLoaded();
        validateHelper.setText(emailInput,emailValue);
        validateHelper.setText(firstnameInput, fname);
        validateHelper.setText(lastnameInput, lname);
        validateHelper.setText(passwordInput,password);
        validateHelper.setText(confirmPassInput,cfpass);
        validateHelper.setText(dateInput, day);
        validateHelper.selectOptionByText(monthInput, month);
        validateHelper.setText(yearInput, year);
        validateHelper.clickElement(maleRadioBtn);
        validateHelper.clickElement(checkbox1);
        validateHelper.clickElement(signupBtn);
    }
}
