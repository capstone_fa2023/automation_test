package anhbttester.pages.UserPage;

import anhbttester.base.ValidateHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class PlaylistDetailPage {
    private WebDriver driver;
    private ValidateHelper validateHelper;
    private By searchBtn = By.xpath("//a[@class='header-button Button-search']//span[@class='svg-icon']//*[name()='svg']");
    private By searchInput = By.xpath("//input[@placeholder='Bạn muốn nghe gì?']");
    private By clickPlaylist = By.xpath("//body/div[@id='root']/div[@class='App']/div[@class='main-content']/div[@id='search']/main[@aria-label='Spotify – Search']/div[@id='searchPage']/div[4]/section[1]/div[2]/div[1]/div[2]");
    private By playBtn = By.xpath("//span[@class='ButtonPlay svg-icon encore-bright-accent-set']//*[name()='svg']//*[name()='path' and contains(@fill,'currentCol')]");
    private By likeBtn = By.xpath("//button[@data-tooltip-id='like-playlist']//*[name()='svg']");
    private By actionBtn = By.xpath("//button[@class='ButtonWrapper option-button']//*[name()='svg']//*[name()='path' and contains(@fill,'currentCol')]");
    private By addQueueBtn = By.xpath("//button[@class='dropdown-item ellipsis-one-line']");
    private By removelibrary = By.xpath("//button[contains(text(),'Xóa khỏi Thư viện')]");
    private By moveFolderBtn = By.xpath("//span[contains(text(),'Di chuyển sang thư mục')]");
    private By createFolderBtn = By.xpath("//body/div[@id='root']/div[@class='App']/div[@class='main-content']/div[@id='scollbar']/div[@class='content']/main[@id='playlist']/div[@class='contentSpacing']/div[@class='action-bar-row']/div[@class='dropdown']/ul[@class='dropdown-menu encore-dark-theme show']/li/ul[@class='dropdown-menu dropdown-submenu']/li/button[@class='dropdown-item']/span[1]");
    private By shareBtn = By.xpath("//body/div[@id='root']/div[@class='App']/div[@class='main-content']/div[@id='scollbar']/div[@class='content']/main[@id='playlist']/div[@class='contentSpacing']/div[@class='action-bar-row']/div[@class='dropdown']/ul[@class='dropdown-menu encore-dark-theme show']/li[4]/button[1]");
    private By copyLinkBtn = By.xpath("//ul[@class='dropdown-menu dropdown-submenu']//button[@class='dropdown-item'][contains(text(),'Sao chép liên kết')]");
    public PlaylistDetailPage(WebDriver driver){
        this.driver = driver;
        validateHelper = new ValidateHelper(driver);
    }
    public void playlistdetail(String input){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(searchBtn);
        validateHelper.waitForPageLoaded();
        validateHelper.setText(searchInput, input);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(clickPlaylist);
    }
    public void playplaylist(){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(playBtn);
    }
    public void likeplaylist(){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(likeBtn);
    }
    public void addqueue(){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(actionBtn);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(addQueueBtn);
    }
    public void removelibrary(){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(actionBtn);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(removelibrary);
    }
    public void addfolder(){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(actionBtn);
        validateHelper.waitForPageLoaded();
        validateHelper.hoverElement(moveFolderBtn);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(createFolderBtn);
    }
    public void shareplaylist(){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(actionBtn);
        validateHelper.waitForPageLoaded();
        validateHelper.hoverElement(shareBtn);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(copyLinkBtn);
    }
}
