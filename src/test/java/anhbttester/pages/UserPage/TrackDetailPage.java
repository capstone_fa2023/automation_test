package anhbttester.pages.UserPage;

import anhbttester.base.ValidateHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import utilities.PropertiesFile;

public class TrackDetailPage {
    private WebDriver driver;
    private ValidateHelper validateHelper;
    private By searchBtn = By.xpath("//a[@class='header-button Button-search']//span[@class='svg-icon']//*[name()='svg']");
    private By searchInput = By.xpath("//input[@placeholder='Bạn muốn nghe gì?']");
    private By clicktrack = By.xpath("//div[@class='list-row'][1]//a[contains(@class, 'text-type')]");
    private By playtrackBtn = By.xpath("//span[@class='ButtonPlay svg-icon encore-bright-accent-set']//*[name()='svg']");
    private By likeBtn = By.xpath("//button[@data-tooltip-id='add-track-to-lb']//*[name()='svg']");
    private By actionBtn = By.xpath("//button[@class='ButtonWrapper option-button']//*[name()='svg']");
    private By actionQueueBtn = By.xpath("//button[@class='dropdown-item ellipsis-one-line']");
    private By removeLibraryBtn = By.xpath("//button[contains(text(),'Xóa kh')]");
    private By addPlaylistBtn = By.xpath("//span[normalize-space()='Thêm vào danh sách phát']");
    private By createPlaylistAction = By.xpath("//button[@class='dropdown-item']//span[contains(text(),'Tạo danh sách phát')]");
    private By shareAction = By.xpath("//body/div[@id='root']/div[@class='App']/div[@class='main-content']/div[@id='scollbar']/div[@class='content']/main[@id='track']/div[@class='contentSpacing']/div[@class='action-bar-row']/div[@class='dropdown']/ul[@class='dropdown-menu encore-dark-theme show']/li[4]/button[1]");
    private By copylinkaction = By.xpath("//button[contains(text(),'Sao chép liên kết')]");
    private By commentInput = By.xpath("//textarea[@placeholder='Viết bình luận...']");
    private By sendCommentBtn = By.xpath("//div[@class='comments-container']//button[@type='submit']//*[name()='svg']");
    private By exitAds = By.xpath("//div[@class='ads-button']");
    public TrackDetailPage(WebDriver driver){
        this.driver = driver;
        validateHelper = new ValidateHelper(driver);
        PropertiesFile.setPropertiesFile();
    }
    public void trackdetail(String search_term){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(searchBtn);
        validateHelper.setText(searchInput,search_term);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(clicktrack);
    }
    public void playtrack(){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(playtrackBtn);
    }
    public void liketrack(){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(likeBtn);
    }
    public void addqueue(){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(actionBtn);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(actionQueueBtn);
    }
    public void removelibrary(){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(actionBtn);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(removeLibraryBtn);
    }
    public void addplaylist(){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(actionBtn);
        validateHelper.waitForPageLoaded();
        validateHelper.hoverElement(addPlaylistBtn);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(createPlaylistAction);
    }
    public void copylink(){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(actionBtn);
        validateHelper.waitForPageLoaded();
        validateHelper.hoverElement(shareAction);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(copylinkaction);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(exitAds);
    }
    public void commentTrack(String comment){
        validateHelper.waitForPageLoaded();
        validateHelper.setText(commentInput, comment);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(sendCommentBtn);
    }
}
