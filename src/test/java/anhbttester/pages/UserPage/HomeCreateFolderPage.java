package anhbttester.pages.UserPage;

import anhbttester.base.ValidateHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class HomeCreateFolderPage {
    private WebDriver driver;
    private ValidateHelper validateHelper;
    private By CreatePlaylistorFolderBtn = By.xpath("//button[@aria-label='Create playlist or folder']//*[name()='svg']//*[name()='path' and contains(@fill,'currentCol')]");
    private By CreateFolderBtn = By.xpath("//span[contains(text(),'Tạo thư mục danh sách phát')]");
    private By FolderElement = By.className("text-name");
    private By filterFolder = By.xpath("//button[@class='ButtonWrapper']//span[contains(text(),'Thư mục')]");
    private By chooseFolder = By.xpath("//div[@class='d-flex align-items-center justify-content-between aside-target']");
    private By reNameFolder = By.xpath("//span[contains(text(),'Đổi tên')]");
    private By reNameInput = By.xpath("//input[@value='New Folder']");
    private By saveBtn = By.xpath("//div[@id='folderModal']//span[@class='ButtonInner encore-inverted-light-set'][contains(text(),'Lưu')]");
    private By deleteFolder = By.xpath("//button[@class='dropdown-item']//span[contains(text(),'Xóa')]");
    private By movetoFolder = By.xpath("//span[contains(text(),'Di chuyển sang thư mục')]");
    private By moveCreateFolder = By.xpath("/html[1]/body[1]/div[1]/div[1]/div[2]/div[1]/nav[1]/div[2]/ul[1]/li[3]/ul[1]/li[1]/button[1]/span[1]");
    public HomeCreateFolderPage(WebDriver driver){
        this.driver = driver;
        validateHelper = new ValidateHelper(driver);
    }
    public void createFolder(){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(CreatePlaylistorFolderBtn);
        validateHelper.clickElement(CreateFolderBtn);
    }
    public int countFolder(){
        try {
            Thread.sleep(5000); // Chờ 5 giây để đảm bảo rằng giao diện đã được cập nhật hoàn toàn
            List<WebElement> folderElements = driver.findElements(By.className("text-name")); // Thay thế bằng biểu thức chính xác
            return folderElements.size();
        } catch (InterruptedException e) {
            System.out.println("InterruptedException occurred: " + e.getMessage());
            return -1; // Hoặc giá trị khác để xử lý trường hợp lỗi
        }
    }
    public void renamefolder(String rename){
//        validateHelper.waitForPageLoaded();
//        validateHelper.clickElement(filterFolder);
        validateHelper.waitForPageLoaded();
        validateHelper.rightClickElement(chooseFolder);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(reNameFolder);
        validateHelper.waitForPageLoaded();
        validateHelper.setText(reNameInput, rename);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(saveBtn);
    }
    public void deletefolder(){
        validateHelper.waitForPageLoaded();
        validateHelper.rightClickElement(chooseFolder);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(deleteFolder);
    }
    public void movefolder(){
        validateHelper.waitForPageLoaded();
        validateHelper.rightClickElement(chooseFolder);
        validateHelper.waitForPageLoaded();
        validateHelper.hoverElement(movetoFolder);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(moveCreateFolder);
    }
}
