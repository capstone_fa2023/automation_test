package anhbttester.pages.UserPage;

import anhbttester.base.ValidateHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ActionTrackPage {
    private WebDriver driver;
    private ValidateHelper validateHelper;
    private By searchBtn = By.xpath("//a[@class='header-button Button-search']//span[@class='svg-icon']//*[name()='svg']");
    private By imgTrackHover = By.xpath("//body/div[@id='root']/div[@class='App']/div[@class='main-content']/div[@id='search']/main[@aria-label='Spotify – Search']/div[@id='searchPage']/div[@class='top-of-result']/section[@class='component-shelf Shelf']/div[@id='vertical-2']/div[@class='grid-container']/div[@class='position-relative']/div[@class='sentinel']/div[1]");
    private By playontrack = By.xpath("//div[@class='sentinel']//div[1]//div[1]//div[1]//button[1]//*[name()='svg']//*[name()='path' and contains(@fill,'currentCol')]");
    private By searchInput = By.xpath("//input[@placeholder='Bạn muốn nghe gì?']");
    private By playBtn = By.xpath("//button[@class='control-button svg-icon control-button-playpause']//*[name()='svg']");
    private By nextBtn = By.xpath("//div[@class='now-playing-bar']//button[4]//*[name()='svg']");
    private By previousBtn = By.xpath("//div[@class='middle-bar']//button[2]");
    private By shuffleBtn = By.xpath("//div[@class='middle-bar']//button[1]//*[name()='svg']");
    private By loopBtn = By.xpath("//div[@class='now-playing-bar']//button[5]//*[name()='svg']");
    private By lyricsBtn = By.xpath("//button[@data-tooltip-id='lyrics']");
    public ActionTrackPage(WebDriver driver)
    {
        this.driver = driver;
        validateHelper = new ValidateHelper(driver);
    }
    public void searchTrack(String nameTrack){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(searchBtn);
        validateHelper.setText(searchInput, nameTrack);
        validateHelper.waitForPageLoaded();
        //validateHelper.clickElement(track);
    }
    public void playtrack(){
        validateHelper.waitForPageLoaded();
        validateHelper.hoverElement(imgTrackHover);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(playontrack);
    }
    public void pausetrack(){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(playBtn);
    }
    public void nexttrack(){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(nextBtn);
    }
    public void previoustrack(){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(previousBtn);
    }
    public void shuffletrack(){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(shuffleBtn);
    }
    public void looptrack(){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(loopBtn);
    }
    public void openLyrics(){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(lyricsBtn);
    }
}
