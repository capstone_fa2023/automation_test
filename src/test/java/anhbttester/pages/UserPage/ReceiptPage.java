package anhbttester.pages.UserPage;

import anhbttester.base.ValidateHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ReceiptPage {
    private WebDriver driver;
    private ValidateHelper validateHelper;
    private By receiptMenu = By.xpath("//span[contains(text(),'Lịch sử đặt hàng')]");
    public ReceiptPage(WebDriver driver){
        this.driver = driver;
        validateHelper = new ValidateHelper(driver);
    }
    public void openReceiptPage(){
        validateHelper.waitForPageLoaded();
        driver.findElement(receiptMenu).click();
    }
}
