package anhbttester.pages.UserPage;

import anhbttester.base.ValidateHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CommunityPage {
    private WebDriver driver;
    private ValidateHelper validateHelper;
    private By searchBtn = By.xpath("//a[@class='header-button Button-search']//span[@class='svg-icon']//*[name()='svg']");
    private By searchCommunity = By.xpath("//span[normalize-space()='Community']");
    private By searchInput = By.xpath("//input[@placeholder='Tìm kiếm']");
    private By selectType = By.xpath("//select[@name='typeObject']");
    private By selectTypeTrack = By.xpath("//option[@value='tracks']");
    private By selectTypePlaylist = By.xpath("//option[@value='playlists']");
    private By selectTypeArtist = By.xpath("//option[@value='artists']");
    private By selectDate = By.xpath("//select[@name='orderDate']");
    private By selectDateNew = By.xpath("//option[contains(text(),'Mới nhất')]");
    private By selectDateOld = By.xpath("//option[contains(text(),'Cũ nhất')]");
    private By selectListens = By.xpath("//select[@name='orderListen']");
    private By selectListensHightoLow = By.xpath("//option[contains(text(),'Lượt nghe cao đến thấp')]");
    private By selectListensLowtoHigh = By.xpath("//option[contains(text(),'Lượt nghe thấp đến cao')]");
    private By selectLikes = By.xpath("//select[@name='orderLikes']");
    private By selectLikesHightoLow = By.xpath("//option[contains(text(),'Lượt thích cao đến thấp')]");
    private By selectLikesLowtoHigh = By.xpath("//option[contains(text(),'Lượt thích thấp đến cao')]");
    public CommunityPage(WebDriver driver){
        this.driver = driver;
        validateHelper = new ValidateHelper(driver);
    }
    public void searchAlbumCommunity(String nameTrack){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(searchBtn);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(searchCommunity);
        validateHelper.setText(searchInput, nameTrack);
        validateHelper.waitForPageLoaded();
    }
    public void searchTrackCommunity(){
        validateHelper.clickElement(selectType);
        validateHelper.clickElement(selectTypeTrack);
    }
    public void searchPlaylistCommunity(){
        validateHelper.clickElement(selectType);
        validateHelper.clickElement(selectTypePlaylist);
    }
    public void searchArtistCommunity(){
        validateHelper.clickElement(selectType);
        validateHelper.clickElement(selectTypeArtist);
    }
    public void selectDateNewCommunity(){
        validateHelper.clickElement(selectDate);
        validateHelper.clickElement(selectDateNew);
    }
    public void selectDateOldCommunity(){
        validateHelper.clickElement(selectDate);
        validateHelper.clickElement(selectDateOld);
    }
    public void selectHighListenCommunity(){
        validateHelper.clickElement(selectListens);
        validateHelper.clickElement(selectListensHightoLow);
    }
    public void selectLowListenCommunity(){
        validateHelper.clickElement(selectListens);
        validateHelper.clickElement(selectListensLowtoHigh);
    }
    public void selectHighLikeCommunity(){
        validateHelper.clickElement(selectLikes);
        validateHelper.clickElement(selectLikesHightoLow);
    }
    public void selectLowLikeCommunity(){
        validateHelper.clickElement(selectLikes);
        validateHelper.clickElement(selectListensLowtoHigh);
    }
}
