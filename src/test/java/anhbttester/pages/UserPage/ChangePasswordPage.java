package anhbttester.pages.UserPage;

import anhbttester.base.ValidateHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ChangePasswordPage {
    private WebDriver driver;
    private ValidateHelper validateHelper;
    private By changePassBtn = By.xpath("//span[contains(text(),'Đổi mật khẩu')]");
    private By currentPassInput = By.xpath("//input[@id='old_password']");
    private By newPassInput = By.xpath("//input[@id='new_password']");
    private By repeatPassInput = By.xpath("//input[@id='new_password_confirmation']");
    private By setNewPassBtn = By.xpath("//button[contains(text(),'Cài đặt mật khẩu mới')]");
public ChangePasswordPage(WebDriver driver){
    this.driver = driver;
    validateHelper = new ValidateHelper(driver);
}
    public void openChangePass(){
        validateHelper.waitForPageLoaded();
        driver.findElement(changePassBtn).click();
    }
    public void changePassword(String currentPass, String newPass, String repeatPass){
        validateHelper.waitForPageLoaded();
        validateHelper.setText(currentPassInput, currentPass);
        validateHelper.setText(newPassInput, newPass);
        validateHelper.setText(repeatPassInput, repeatPass);
        validateHelper.clickElement(setNewPassBtn);
    }
}
