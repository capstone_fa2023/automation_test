package anhbttester.pages.UserPage;

import anhbttester.base.ValidateHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class NotificationPage {
    private WebDriver driver;
    private ValidateHelper validateHelper;
    private By clickIconNoti = By.xpath("//a[@class='ButtonWrapper header-button encore-over-media-set']//*[name()='svg']");
    public NotificationPage(WebDriver driver){
        this.driver = driver;
        validateHelper = new ValidateHelper(driver);
    }
    public void viewnotification(){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(clickIconNoti);
    }
}
