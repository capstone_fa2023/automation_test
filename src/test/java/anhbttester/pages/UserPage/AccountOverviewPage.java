package anhbttester.pages.UserPage;

import anhbttester.base.ValidateHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class AccountOverviewPage {
    private WebDriver driver;
    private ValidateHelper validateHelper;
    private By editProfileBtn = By.xpath("//button[@class='ButtonWrapper Button-sc']");
    private By fnameInput = By.xpath("//input[@id='fname']");
    private By lnameInput = By.xpath("//input[@id='lname']");
    private By genderSelect = By.xpath("//select[@id='gender']");
    private By dayInput = By.xpath("//input[@placeholder='DD']");
    private By monthSelect = By.xpath("//select[@name='month']");
    private By yearInput = By.xpath("//input[@id='year']");
    private By saveBtn = By.xpath("//button[@class='ButtonWrapper Button-sc']");
    public AccountOverviewPage(WebDriver driver){
        this.driver = driver;
        validateHelper = new ValidateHelper(driver);
    }
    public void openEditProfile(){
        validateHelper.waitForPageLoaded();
        driver.findElement(editProfileBtn).click();
    }
    public void editProfile(String fname, String lname){
        validateHelper.waitForPageLoaded();
        validateHelper.setText(fnameInput, fname);
        validateHelper.setText(lnameInput, lname);
        validateHelper.clickElement(saveBtn);
    }
}
