package anhbttester.pages.UserPage;

import anhbttester.base.ValidateHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class HomeCreatePlaylistPage {
    private WebDriver driver;
    private ValidateHelper validateHelper;
    private By filterPlaylist = By.xpath("//button[@class='ButtonWrapper']//span[contains(text(),'Danh sách phát')]");
    private By CreatePlaylistorFolderBtn = By.xpath("//button[@aria-label='Create playlist or folder']//*[name()='svg']");
    private By CreatePlaylistBtn = By.xpath("//span[contains(text(),'Tạo danh sách phát mới')]");
    private By choosePlaylist = By.xpath("//div[@class='list-item active']");
    private By addToQueueBtn = By.xpath("//button[@class='dropdown-item'][contains(text(),'Thêm vào danh sách chờ')]");
    private By addToProfileBtn = By.xpath("//button[contains(text(),'Thêm vào hồ sơ')]");
    private By pinPlaylist = By.xpath("//span[normalize-space()='Ghim danh sách phát']");
    private By deletePlaylistBtn = By.xpath("//button[@class='dropdown-item']//span[contains(text(),'Xóa')]");
    private By confirmDeleteBtn = By.xpath("//span[normalize-space()='Xóa']");
    private By editInfor = By.xpath("//span[contains(text(),'Sửa thông tin chi tiết')]");
    private By movetofolderBtn = By.xpath("//ul[@class='dropdown-menu encore-dark-theme show']//span[contains(text(),'Di chuyển sang thư mục')]");
    private By movecreatefolder = By.xpath("//body/div[@id='root']/div[@class='App']/div[@class='main-content']/div[@class='left-sidebar']/nav[@class='nav-sidebar']/div[@id='context-menu']/ul[@class='dropdown-menu encore-dark-theme show']/li/ul[@class='dropdown-menu dropdown-submenu']/li/button[@class='dropdown-item']/span[1]");
    public HomeCreatePlaylistPage(WebDriver driver)
    {
        this.driver = driver;
        validateHelper = new ValidateHelper(driver);
    }
    public void createPlaylist(){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(CreatePlaylistorFolderBtn);
        validateHelper.clickElement(CreatePlaylistBtn);
    }
    public int countPlaylists() {
        try {
            Thread.sleep(5000); // Chờ 5 giây để đảm bảo rằng giao diện đã được cập nhật hoàn toàn
            List<WebElement> playlistElements = driver.findElements(By.className("text-name")); // Thay thế bằng biểu thức chính xác
            return playlistElements.size();
        } catch (InterruptedException e) {
            System.out.println("InterruptedException occurred: " + e.getMessage());
            return -1; // Hoặc giá trị khác để xử lý trường hợp lỗi
        }
    }

    public void addtoqueue(){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(filterPlaylist);
        validateHelper.waitForPageLoaded();
        validateHelper.rightClickElement(choosePlaylist);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(addToQueueBtn);
    }

    public void addtoprofile(){
        validateHelper.waitForPageLoaded();
        validateHelper.rightClickElement(choosePlaylist);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(addToProfileBtn);
    }
    public void pinplaylist(){
        validateHelper.waitForPageLoaded();
        validateHelper.rightClickElement(choosePlaylist);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(pinPlaylist);
    }

    public void editplaylist(){
        validateHelper.waitForPageLoaded();
        validateHelper.rightClickElement(choosePlaylist);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(editInfor);
    }

    public void movefolder(){
        validateHelper.waitForPageLoaded();
        validateHelper.rightClickElement(choosePlaylist);
        validateHelper.waitForPageLoaded();
        validateHelper.hoverElement(movetofolderBtn);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(movecreatefolder);
    }

    public void deleteplaylist(){
        validateHelper.waitForPageLoaded();
        validateHelper.rightClickElement(choosePlaylist);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(deletePlaylistBtn);
        validateHelper.waitForPageLoaded();
        //validateHelper.clickElement(confirmDeleteBtn);
        WebElement deleteButton = driver.findElement(By.xpath("//span[normalize-space()='Xóa']"));
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", deleteButton);
    }

}
