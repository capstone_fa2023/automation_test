package anhbttester.pages.UserPage;

import anhbttester.base.ValidateHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class RecoverPlaylistPage {
    private WebDriver driver;
    private ValidateHelper validateHelper;
    private By filterPlaylist = By.xpath("//button[@class='ButtonWrapper']//span[contains(text(),'Danh sách phát')]");
    private By choosePlaylist = By.xpath("//div[@class='list-item active']//div[@class='d-flex align-items-center justify-content-between aside-target']");
    private By deletePlaylistBtn = By.xpath("//button[@class='dropdown-item']//span[contains(text(),'Xóa')]");
    private By confirmDeleteBtn = By.cssSelector("div[id='delConfirm'] button:nth-child(2)");
    private By recoverMenu = By.xpath("//span[contains(text(),'Khôi phục danh sách phát')]");
    private By restoreBtn = By.xpath("//div[@class='grid-container']//div[1]//div[4]//button[1]//span[1]");
    public RecoverPlaylistPage(WebDriver driver){
        this.driver = driver;
        validateHelper = new ValidateHelper(driver);
    }
    public void deleteplaylist(){
        validateHelper.waitForPageLoaded();
        validateHelper.rightClickElement(choosePlaylist);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(deletePlaylistBtn);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(confirmDeleteBtn);

    }
    public void openRecoverPage(){
        validateHelper.waitForPageLoaded();
        driver.findElement(recoverMenu).click();
    }
    public void restoreplaylist(){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(restoreBtn);
    }
}
