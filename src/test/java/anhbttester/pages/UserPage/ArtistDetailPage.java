package anhbttester.pages.UserPage;

import anhbttester.base.ValidateHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.TimeoutException;
import org.testng.Assert;

public class ArtistDetailPage {
    private WebDriver driver;
    private ValidateHelper validateHelper;
    private By searchBtn = By.xpath("//a[@class='header-button Button-search']//span[@class='svg-icon']//*[name()='svg']");
    private By searchInput = By.xpath("//input[@placeholder='Bạn muốn nghe gì?']");
    private By clickArtist = By.xpath("//div[@class='top-result-card']//div[@class='card-click-handler']");
    private By playBtn = By.xpath("//span[@class='ButtonPlay svg-icon encore-bright-accent-set']//*[name()='svg']//*[name()='path' and contains(@fill,'currentCol')]");
    private By followBtn = By.xpath("//button[@class='ButtonWrapper artist-button']");
    private By overviewBtn =By.xpath("//button[contains(text(),'Tổng quan')]");
    private By postArtist = By.xpath("//button[contains(text(),'Bài viết')]");
    private By commentInput = By.xpath("//body/div[@id='root']/div[@class='App']/div[@class='main-content']/div[@id='scollbar']/div[@class='content']/main[@id='artist']/div[@class='contentSpacing']/div[@id='loader']/div[@class='post-container']/div[@class='row']/div[@class='col-lg-5']/div[@class='post-comments']/form[@class='comment-area']/textarea[1]");
    private By sendCommentBtn = By.xpath("//body/div[@id='root']/div[@class='App']/div[@class='main-content']/div[@id='scollbar']/div[@class='content']/main[@id='artist']/div[@class='contentSpacing']/div[@id='loader']/div[@class='post-container']/div[@class='row']/div[@class='col-lg-5']/div[@class='post-comments']/form[@class='comment-area']/div[@class='d-flex align-items-center justify-content-end gap-3']/button[1]//*[name()='svg']");
    private By exitAds = By.xpath("//div[@class='ads-button']");
    private By actionMoreBtn = By.xpath("//button[@class='ButtonWrapper option-button']//*[name()='svg']");
    private By unfollowActionBtn = By.xpath("//button[@class='dropdown-item ellipsis-one-line']");
    private By reportActionBtn = By.xpath("//span[normalize-space()='Báo cáo']");
    private By reportProfilePhoto = By.xpath("//span[contains(text(),'Báo cáo ảnh hồ sơ')]");
    private By realArtistPhotoInput = By.xpath("//div[@id='profilePhotoModal']//input[@id='artist']");
    private By selectRealArtist = By.xpath("//div[@id='profilePhotoModal']//div[@class='result-box']//div[1]");
    private By sendPhotoReportBtn = By.xpath("//div[@id='profilePhotoModal']//span[@class='ButtonInner encore-inverted-light-set '][contains(text(),'Gửi')]");
    private By reportPageImage = By.xpath("//span[contains(text(),'Báo cáo hình ảnh trang')]");
    private By realArtistImgInput = By.xpath("//div[@id='imgModal']//input[@id='artist']");
    private By selectRealArtistImg = By.xpath("//div[@id='imgModal']//div[@class='result-box']//div[1]");
    private By sendReportImgBtn = By.xpath("//div[@id='imgModal']//span[@class='ButtonInner encore-inverted-light-set '][contains(text(),'Gửi')]");
    private By userReport = By.xpath("//span[normalize-space()='Báo cáo nghệ sĩ']");
    private By realArtistUserRpInput = By.xpath("//div[@id='userModal']//input[@id='artist']");
    private By selectRealArtistUR = By.xpath("//div[@id='userModal']//div[@class='result-box']//div[1]");
    private By sendUserRPBtn = By.xpath("//div[@id='userModal']//span[@class='ButtonInner encore-inverted-light-set '][normalize-space()='Gửi']");
    private By shareActionBtn = By.xpath("//span[contains(text(),'Chia sẻ')]");
    private By copyLink = By.xpath("//button[contains(text(),'Sao chép liên kết')]");

    public ArtistDetailPage(WebDriver driver){
        this.driver = driver;
        validateHelper = new ValidateHelper(driver);
    }
    public void artistdetail(String input){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(searchBtn);
        validateHelper.waitForPageLoaded();
        validateHelper.setText(searchInput, input);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(clickArtist);
    }
    public void playinartistdetail(){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(playBtn);
    }
    public void followArtist(){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(followBtn);
    }
    public void overviewArtist(){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(overviewBtn);
    }
    public void commentPost(String comment){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(postArtist);
        validateHelper.clickElement(exitAds);
        try {
            validateHelper.waitForElementToBeClickable(commentInput, 5);
        validateHelper.setText(commentInput, comment);
        //validateHelper.waitForElementToBeClickable(sendCommentBtn, 5);
        validateHelper.clickElement(sendCommentBtn);
        } catch (TimeoutException e) {
            Assert.fail("Element not found within the specified timeout.");
        }
    }
    public void unfollow(){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(actionMoreBtn);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(unfollowActionBtn);
    }
    public void copylinkartist(){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(actionMoreBtn);
        validateHelper.waitForPageLoaded();
        validateHelper.hoverElement(shareActionBtn);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(copyLink);
    }
    public void openreport(){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(actionMoreBtn);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(reportActionBtn);
    }
    public void processreportphoto(String input){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(reportProfilePhoto);
        validateHelper.waitForPageLoaded();
        validateHelper.setText(realArtistPhotoInput, input);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(selectRealArtist);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(sendPhotoReportBtn);
    }
    public void processreportimage(String input){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(reportPageImage);
        validateHelper.waitForPageLoaded();
//        validateHelper.setText(realArtistImgInput, input);
//        validateHelper.waitForPageLoaded();
//        validateHelper.clickElement(selectRealArtistImg);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(sendReportImgBtn);
    }
    public void processuserrp(String input){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(userReport);
        validateHelper.waitForPageLoaded();
//        validateHelper.setText(realArtistUserRpInput, input);
//        validateHelper.waitForPageLoaded();
//        validateHelper.clickElement(selectRealArtistUR);
//        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(sendUserRPBtn);
    }
}
