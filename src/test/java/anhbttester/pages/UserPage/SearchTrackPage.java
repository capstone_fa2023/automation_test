package anhbttester.pages.UserPage;

import anhbttester.base.ValidateHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class SearchTrackPage {
    private WebDriver driver;
    private ValidateHelper validateHelper;
    private By searchBtn = By.xpath("//a[@class='header-button Button-search']//span[@class='svg-icon']//*[name()='svg']");
    private By searchInput = By.xpath("//input[@placeholder='Bạn muốn nghe gì?']");
    private By filterArtists = By.xpath("//span[contains(text(),'Nghệ sĩ')]");
    private By filterTracks = By.xpath("//span[normalize-space()='Bài hát']");
    private By filterAlbums = By.xpath("//span[normalize-space()='Album']");
    private By filterPlaylist = By.xpath("//span[normalize-space()='Danh sách phát']");
    public SearchTrackPage(WebDriver driver)
    {
        this.driver = driver;
        validateHelper = new ValidateHelper(driver);
    }
    public void searchTrack(String nameTrack){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(searchBtn);
        validateHelper.setText(searchInput, nameTrack);
        validateHelper.waitForPageLoaded();
        //validateHelper.clickElement(track);
    }
    public void searchartist(){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(filterArtists);
    }
    public void searchtrack(){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(filterTracks);
    }
    public void searchalbum(){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(filterAlbums);
    }
    public void searchplaylist(){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(filterPlaylist);
    }
}
