package anhbttester.pages.UserPage;

import anhbttester.base.ValidateHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class AlbumDetailPage {
    private WebDriver driver;
    private ValidateHelper validateHelper;
    private By searchBtn = By.xpath("//a[@class='header-button Button-search']//span[@class='svg-icon']//*[name()='svg']");
    private By searchInput = By.xpath("//input[@placeholder='Bạn muốn nghe gì?']");
    private By clickAlbum = By.xpath("//body/div[@id='root']/div[@class='App']/div[@class='main-content']/div[@id='search']/main[@aria-label='Spotify – Search']/div[@id='searchPage']/div[3]/section[1]/div[2]/div[1]/div[2]");
    private By playAlbumBtn = By.xpath("//span[@class='ButtonPlay svg-icon encore-bright-accent-set']");
    private By likeAlbumBtn = By.xpath("//button[@data-tooltip-id='like-album']//*[name()='svg']");
    private By actionBtn = By.xpath("//button[@class='ButtonWrapper option-button']//*[name()='svg']");
    private By addQueueBtn = By.xpath("//button[@class='dropdown-item ellipsis-one-line']");
    private By removeLibraryBtn = By.xpath("//button[contains(text(),'Xóa khỏi Thư viện')]");
    private By addPlaylistBtn = By.xpath("//ul[@class='dropdown-menu encore-dark-theme show']//span[contains(text(),'Thêm vào danh sách phát')]");
    private By createPlaylistBtn = By.xpath("//ul[@class='dropdown-menu encore-dark-theme show']//span[contains(text(),'Tạo danh sách phát')]");
    private By shareBtn = By.xpath("//body/div[@id='root']/div[@class='App']/div[@class='main-content']/div[@id='scollbar']/div[@class='content']/main[@id='album']/div[@class='contentSpacing']/div[@class='action-bar-row']/div[@class='dropdown']/ul[@class='dropdown-menu encore-dark-theme show']/li[4]/button[1]");
    private By copyLinkBtn = By.xpath("//ul[@class='dropdown-menu dropdown-submenu']//button[@class='dropdown-item'][contains(text(),'Sao chép liên kết')]");
    public AlbumDetailPage(WebDriver driver){
        this.driver = driver;
        validateHelper = new ValidateHelper(driver);
    }
    public void albumdetail(String input){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(searchBtn);
        validateHelper.waitForPageLoaded();
        validateHelper.setText(searchInput, input);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(clickAlbum);
    }
    public void playalbum(){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(playAlbumBtn);
    }
    public void likealbum(){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(likeAlbumBtn);
    }
    public void addqueue(){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(actionBtn);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(addQueueBtn);
    }
    public void removelibrary(){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(actionBtn);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(removeLibraryBtn);
    }
    public void addplaylist(){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(actionBtn);
        validateHelper.waitForPageLoaded();
        validateHelper.hoverElement(addPlaylistBtn);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(createPlaylistBtn);
    }
    public void sharealbum(){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(actionBtn);
        validateHelper.waitForPageLoaded();
        validateHelper.hoverElement(shareBtn);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(copyLinkBtn);
    }
}
