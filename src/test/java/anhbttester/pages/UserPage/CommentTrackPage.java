package anhbttester.pages.UserPage;

import anhbttester.base.ValidateHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

public class CommentTrackPage {
    private WebDriver driver;
    private ValidateHelper validateHelper;
    private By searchBtn = By.xpath("//a[@class='header-button Button-search']//span[@class='svg-icon']//*[name()='svg']");
    private By track = By.xpath("//a[normalize-space()='Mars']");
    private By searchInput = By.xpath("//input[contains(@placeholder,'Bạn muốn nghe gì?')]");
    private By commentInput = By.xpath("//textarea[@placeholder='Viết bình luận...']");
    private By sendcmtBtn = By.xpath("//div[@class='comments-container']//button[@type='submit']//*[name()='svg']");
    private By commentElement = By.className("comment-renderer");
    public CommentTrackPage(WebDriver driver){
        this.driver = driver;
        validateHelper = new ValidateHelper(driver);
    }
    public void searchTrack(String nameTrack){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(searchBtn);
        validateHelper.setText(searchInput, nameTrack);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(track);
    }
    public void commentTrack(String comment){
        validateHelper.waitForPageLoaded();
        validateHelper.setText(commentInput, comment);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(sendcmtBtn);
    }
    public int countComment() {
        try {
            Thread.sleep(5000);
            List<WebElement> commentElements = driver.findElements(By.className("comment-renderer"));
            return commentElements.size();
        } catch (InterruptedException e) {
            System.out.println("InterruptedException occurred: " + e.getMessage());
            return -1; // Hoặc giá trị khác để xử lý trường hợp lỗi
        }
    }
}
