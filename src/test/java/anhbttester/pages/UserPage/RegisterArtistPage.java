package anhbttester.pages.UserPage;

import anhbttester.base.ValidateHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class RegisterArtistPage {
    private WebDriver driver;
    private ValidateHelper validateHelper;
    private By avtImge = By.xpath("//img[@class='rounded-circle']");
    private By forArtist = By.xpath("//span[contains(text(),'Dành cho nghệ sĩ')]");
    private By claimBtn = By.xpath("//a[@class='claim__Button']");
    private By nextBtn = By.xpath("//span[@class='artist-access-button next encore-bright-accent-set']");
    private By stageNameInput = By.xpath("//input[@id='stageName']");
    private By selectBank = By.xpath("//select[@id='bankCode']");
    private By accountNumberInput = By.xpath("//input[@id='bankAccount']");
    private By soicalLinkInput = By.xpath("//body/div[@id='root']/div[@class='App']/div[@id='artist-claim']/main[@class='Main__Wrapper']/div[@class='claim__container']/div[@class='claim__MainCenterer']/form[@class='px-3']/div[@class='Group-sc']/div[2]/input[1]");
    private By descriptionInput = By.xpath("//textarea[@id='description']");
    private By finishBtn = By.xpath("//span[@class='artist-access-button next encore-bright-accent-set ']");
    public RegisterArtistPage (WebDriver driver){
        this.driver = driver;
        validateHelper = new ValidateHelper(driver);
    }
    public void openFormRegister(){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(avtImge);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(forArtist);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(claimBtn);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(nextBtn);
    }
    public void fillInforRegis(String stage_name,String bank_code, String account_number, String social_link, String description ){
        validateHelper.waitForPageLoaded();
        validateHelper.setText(stageNameInput, stage_name);
        validateHelper.waitForPageLoaded();
        validateHelper.selectOptionByText(selectBank, bank_code);
        validateHelper.waitForPageLoaded();
        validateHelper.setText(accountNumberInput, account_number);
        validateHelper.waitForPageLoaded();
        validateHelper.setText(soicalLinkInput, social_link);
        validateHelper.waitForPageLoaded();
        validateHelper.setText(descriptionInput, description);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(finishBtn);

    }
}
