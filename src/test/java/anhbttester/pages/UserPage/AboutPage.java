package anhbttester.pages.UserPage;

import anhbttester.base.ValidateHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class AboutPage {
    private WebDriver driver;
    private ValidateHelper validateHelper;
    private By avtImge = By.xpath("//img[@class='rounded-circle']");
    private By forAbout = By.xpath("//a[@href='/about-us/contact/']");
    public AboutPage (WebDriver driver){
        this.driver = driver;
        validateHelper = new ValidateHelper(driver);
    }
    public void openaboutpage(){
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(avtImge);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElement(forAbout);
    }
}
