package anhbttester.base;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;

import java.util.concurrent.TimeUnit;

public class BaseSetup {
    private static WebDriver driver;
    public static WebDriver getDriver(){
        return driver;
    }

    public WebDriver setupDriver(String browserType) {
        switch (browserType.trim().toLowerCase()) {
            case "chrome":
                driver = initChromeDriver();
                break;
            case "firefox":
                driver = initFirefoxDriver();
                break;
            case "opera":
                driver = initOperaDriver();
                break;
            case "edge":
                driver = initEdgeDriver();
                break;
            default:
                System.out.println("Browser: " + browserType + " is invalid, Launching Chrome as browser of choice...");
                driver = initChromeDriver();
        }
        return driver;
    }

    // Hàm này để tùy chọn Browser. Cho chạy trước khi gọi class này (BeforeClass)
    private void setDriver(String browserType, String appURL) {
        switch (browserType) {
            case "chrome":
                driver = initChromeDriver();
                driver.navigate().to(appURL);
                break;
            case "firefox":
                driver = initFirefoxDriver();
                driver.navigate().to(appURL);
                break;
            case "opera":
                driver = initOperaDriver();
                driver.navigate().to(appURL);
                break;
            default:
                System.out.println("Browser: " + browserType + " is invalid, Launching Chrome as browser of choice...");
                driver = initChromeDriver();
        }
    }

    // Khởi tạo cấu hình của các Browser để đưa vào Switch Case
    private WebDriver initChromeDriver() {
        System.out.println("Launching Chrome browser...");
//        WebDriverManager.chromedriver().setup();
//        WebDriverManager.chromedriver().driverVersion("119").setup();
//        driver = new ChromeDriver();
//        driver.manage().window().maximize();
//        driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
//        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        ChromeOptions options=new ChromeOptions();
        options.addArguments("--headless");
        options.addArguments("--disable-dev-shm-usage");
        driver=new ChromeDriver(options);
        return driver;
    }

    private WebDriver initEdgeDriver() {
        System.out.println("Launching Edge browser...");
        WebDriverManager.edgedriver().setup();
        //driver = new EdgeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        return driver;
    }

    private WebDriver initFirefoxDriver() {
        System.out.println("Launching Firefox browser...");
        WebDriverManager.firefoxdriver().setup();
        //driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        return driver;
    }

    private WebDriver initOperaDriver() {
        System.out.println("Launching Opera browser...");
        WebDriverManager.operadriver().setup();
        //driver = new OperaDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        return driver;
    }

    // Chạy hàm initializeTestBaseSetup trước hết khi class này được gọi
    @Parameters({"browserType", "webURL"})
    @BeforeClass
    public void initializeTestBaseSetup(String browserType,String webURL) {
        try {
            // Khởi tạo driver và tùy chọn browser và web url
            setDriver(browserType, webURL);
        } catch (Exception e) {
            System.out.println("Error..." + e.getStackTrace());
        }
    }

    @AfterClass
    public void tearDown() throws Exception {
        Thread.sleep(2000);
        if(driver != null){
            driver.quit();
        }
    }



    //--------------------------------------------------------------------------
//    private void setDriver(String browserType, String appURL){
//        switch (browserType){
//            case "chrome":
//                driver = initChromeDriver(appURL);
//                break;
//            case "gecko":
//                driver = initFirefoxDriver(appURL);
//                break;
//            default:
//                System.out.println("Browser:" + browserType + "is valid, Lauching Chrome as browser of choice ");
//                driver = initChromeDriver(appURL);
//        }
//    }
//    //Khởi tạo cấu hình của browser để đưa vào switch case
//    private WebDriver initChromeDriver(String appURL){
//        System.out.println("Launching Chrome browser...");
//       System.setProperty("webdriver.chorme.driver", "E:\\Capstone 2023\\tool\\chromedriver.exe");
//        driver = new ChromeDriver();
//        driver.manage().window().maximize();
//        driver.navigate().to(appURL);
//        driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
//        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
//        return driver;
//    }
//
//    private WebDriver initFirefoxDriver(String appURL){
//        System.out.println("Launching Firefox browser...");
//        System.setProperty("webdriver.gecko.driver", "E:\\Capstone 2023\\tool\\chromedriver.exe");
//        WebDriverManager.chromedriver().setup();
//        driver = new ChromeDriver();
//        driver.manage().window().maximize();
//        driver.navigate().to(appURL);
//        driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
//        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
//        return driver;
//    }
//    @Parameters({"browserType", "appURL"})
//    @BeforeClass
//    public void initializeTestBaseSetup(String browserType,@Optional String appURL){
//        try {
//            setDriver(browserType, appURL);
//        }catch (Exception e){
//            System.out.println("Error..." + e.getStackTrace());
//        }
//    }
//    @AfterClass
//    public void tearDown()throws Exception{
//        Thread.sleep(2000);
//        driver.quit();
//    }
    //-------------------------------------------------------------------------------------

}
