package anhbttester.base;

import org.apache.hc.client5.http.classic.HttpClient;
import org.apache.hc.client5.http.classic.methods.HttpGet;
import org.apache.hc.client5.http.impl.classic.HttpClientBuilder;

import org.apache.hc.core5.http.ClassicHttpResponse;
import org.apache.hc.core5.http.HttpResponse;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.time.Duration;
import java.util.List;

public class ValidateHelper {
    private WebDriver driver;
    private Actions action;
    private WebDriverWait wait;
    public ValidateHelper(WebDriver driver)
    {
        this.driver = driver;
        Duration timeout = Duration.ofSeconds(5000);
        wait = new WebDriverWait(driver, timeout);
        action = new Actions(driver);
    }

    public void hoverElement(By element){
        waitForPageLoaded();
        WebElement webElement = wait.until(ExpectedConditions.elementToBeClickable(element));
        action.moveToElement(webElement).perform();
    }

    public void waitForElementToBeClickable(By element, int timeoutInSeconds) {
        wait.withTimeout(Duration.ofSeconds(timeoutInSeconds));
        wait.until(ExpectedConditions.elementToBeClickable(element));
    }

    public void setText(By element, String value)
    {
        waitForPageLoaded();
        WebElement webElement = driver.findElement(element);

        // Scroll tới phần tử nếu nó không hiển thị trên màn hình
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", webElement);
        wait.until(ExpectedConditions.elementToBeClickable(element));
        driver.findElement(element).clear();
        driver.findElement(element).sendKeys(value);
    }
    public void setTextArea(By element, String value) {
        try {
            // Chờ đến khi phần tử có thể tương tác được
            WebElement webElement = wait.until(ExpectedConditions.elementToBeClickable(element));

            // Click vào phần tử trước đó
            action.moveToElement(webElement).click().build().perform();

            // Sử dụng JavascriptExecutor để set giá trị của phần tử
            JavascriptExecutor executor = (JavascriptExecutor) driver;
            executor.executeScript("arguments[0].value = arguments[1];", webElement, value);
        } catch (Exception e) {
            // Xử lý nếu có lỗi (in hoặc log lỗi)
            System.err.println("Error while setting text: " + e.getMessage());
        }
    }

    public String getElementText(By element) {
        List<WebElement> webElements = driver.findElements(element);
        if (webElements.isEmpty()) {
            System.out.println("Element not found");
            return ""; // Return an empty string if the element is not found
        } else {
            // Assuming you want the text of the first matching element
            return webElements.get(0).getText();
        }
    }

    public void clickElement(By element)
    {
        waitForPageLoaded();
        wait.until(ExpectedConditions.elementToBeClickable(element));
        driver.findElement(element).click();
    }

    public void rightClickElement(By element){
        WebElement targetElement = wait.until(ExpectedConditions.elementToBeClickable(element));
        Actions action = new Actions(driver);
        action.moveToElement(targetElement).contextClick().build().perform();
    }

    public void selectOptionByText(By element, String text)
    {
        waitForPageLoaded();
        Select select = new Select(driver.findElement(element));
        select.selectByVisibleText(text);
    }
//DropdownOption

    public boolean isElementPresent(By element) {
        return driver.findElements(element).size() > 0;
    }

    public void clickElementByJs(By element) {
        WebElement webElement = driver.findElement(element);
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", webElement);
    }

    public void selectOptionInMenu(By menuElement, String optionText) {
        clickElement(menuElement);

        By optionLocator = By.xpath("//li[text()='" + optionText + "']");
        clickElement(optionLocator);
    }

    public By createOptionLocator(String dropdownId, String text) {
    return By.xpath(String.format("//div[@id='%s']//div[text()='%s']", dropdownId, text));
    }
    public WebElement waitForElementToBeClickable(By locator) {
        Duration timeout = Duration.ofSeconds(10);
        WebDriverWait wait = new WebDriverWait(driver, timeout);
        return wait.until(ExpectedConditions.elementToBeClickable(locator));
    }
/////
    public void selectOptionByValue(By element, String value)
    {
        waitForPageLoaded();
        Select select = new Select(driver.findElement(element));
        select.selectByValue(value);
    }

    public void selectOptionByIndex(By element, String index)
    {
        waitForPageLoaded();
        Select select = new Select(driver.findElement(element));
        select.selectByValue(index);
    }

    public void verifyOptionTotal(By element, int total){
        Select select = new Select(driver.findElement(element));
        Assert.assertEquals(total, select.getOptions().size());
    }

    public void waitForPageLoaded() {
        ExpectedCondition<Boolean> expectation = new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver driver) {
                return ((JavascriptExecutor) driver).executeScript("return document.readyState").toString().equals("complete");
            }
        };
        try {
            Duration timeoutWaitForPageLoaded = Duration.ofSeconds(5000);
            WebDriverWait wait = new WebDriverWait(driver, timeoutWaitForPageLoaded);
            wait.until(expectation);
        }catch (Throwable error){
            Assert.fail("Timeout waiting for Page Load request");
        }
    }
    public boolean verifyUrl(String url) {
//        System.out.println(driver.getCurrentUrl());
//        System.out.println(url);
        return  driver.getCurrentUrl().contains(url);
    }

    public boolean verifyElementText(By element, String textValue){
        wait.until(ExpectedConditions.visibilityOfElementLocated(element));
        List<WebElement> webElements = driver.findElements(element);
        for (WebElement webElement : webElements) {
            String elementText = webElement.getText();
            //System.out.println("Text of the element: " + elementText);
            if (elementText.equalsIgnoreCase(textValue)) {
                return true;
            }
        }
        return false;
    }

    public boolean isLoggedIn() {
        String url = "http://localhost:3000/auth/login";

        HttpClient client = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet(url);
        try {
            ClassicHttpResponse response = (ClassicHttpResponse) client.execute(request);
            int statusCode = response.getCode();
            if (statusCode == 200) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            System.out.println("Error while checking login status: " + e.getMessage());
            return false; // Trả về false nếu có lỗi xảy ra
        }
    }







}
