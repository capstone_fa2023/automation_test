package anhbttester.testcase.admin;

import anhbttester.base.BaseSetup;
import anhbttester.base.ValidateHelper;
import anhbttester.pages.AdminPage.LoginAdminPage;
import anhbttester.pages.AdminPage.ManageArtistPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utilities.PropertiesFile;

import java.io.IOException;

public class ManageArtistTest {
    private WebDriver driver;
    private ValidateHelper validateHelper;
    private LoginAdminPage loginAdminPage;
    private ManageArtistPage manageArtistPage;
    private By changLanguage = By.xpath("//button[@class='MuiButtonBase-root MuiButton-root MuiButton-text MuiButton-textPrimary MuiButton-sizeMedium MuiButton-textSizeMedium MuiButton-disableElevation MuiButton-root MuiButton-text MuiButton-textPrimary MuiButton-sizeMedium MuiButton-textSizeMedium MuiButton-disableElevation css-j0p9p']");
    private By enLanguage = By.xpath("//li[normalize-space()='English']");
    @BeforeClass
    public void setupBrowser() {
        PropertiesFile.setPropertiesFile();
        driver = new BaseSetup().setupDriver(PropertiesFile.getPropValue("browser"));
        //driver = getDriver();
    }
    @Test(priority = 1, description = "View Account Artist")
    //Login success
    public void ArtistDetail() throws InterruptedException, IOException {
        loginAdminPage = new LoginAdminPage(driver);
        validateHelper = new ValidateHelper(driver);
        driver.get(PropertiesFile.getPropValue("homeAdminUrl"));
        String expectedUrl = PropertiesFile.getPropValue("expectHomeAdminUrl");

        try {
            loginAdminPage.loginAdmin(PropertiesFile.getPropValue("emailAdmin"), PropertiesFile.getPropValue("passwordAdmin"));
            Thread.sleep(2000);
            String currentUrl = driver.getCurrentUrl();
            Assert.assertTrue(currentUrl.equals(expectedUrl), "The login details are incorrect");
        } catch (Exception e) {
            System.out.println("Login failed due to exception: " + e.getMessage());
            Assert.fail("Login failed due to exception: " + e.getMessage());
        }
        Thread.sleep(2000);
        validateHelper.clickElementByJs(changLanguage);
        validateHelper.clickElementByJs(enLanguage);
        manageArtistPage = new ManageArtistPage(driver);
        manageArtistPage.seemoreaccount();
        Thread.sleep(2000);
    }
    @Test(priority = 2, description = "View Artist post")
    public void ArtistPost() throws InterruptedException, IOException {
        manageArtistPage = new ManageArtistPage(driver);
        manageArtistPage.seemoreartistpost();
        Thread.sleep(2000);

    }
    @Test(priority = 3, description = "View Statistic Artist")
    public void StatisticArtist() throws InterruptedException, IOException {
        manageArtistPage = new ManageArtistPage(driver);
        manageArtistPage.viewstatistic();
        Thread.sleep(2000);
    }
//    @Test(priority = 4, description = "Deactive")
//    public void DeactiveorActive() throws InterruptedException, IOException {
//        manageArtistPage = new ManageArtistPage(driver);
//        manageArtistPage.deactiveOrActivateUser(PropertiesFile.getPropValue("typeDeactiveArtist"), PropertiesFile.getPropValue("reasonDeactiveorActiveArtist"));
//        Thread.sleep(2000);
//    }
    @AfterClass
    public void closeBrowser() throws Exception {
        Thread.sleep(4000);
        driver.close();
    }
}
