package anhbttester.testcase.admin;

import anhbttester.base.BaseSetup;
import anhbttester.base.ValidateHelper;
import anhbttester.pages.AdminPage.LoginAdminPage;
import anhbttester.pages.AdminPage.ManagePlanBasePage;
import anhbttester.pages.AdminPage.ManagePlanTypePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utilities.PropertiesFile;

import java.io.IOException;

public class ManagePlanTypeTest {
    private WebDriver driver;
    private ValidateHelper validateHelper;
    private ManagePlanTypePage managePlanTypePage;
    private LoginAdminPage loginAdminPage;
    private By changLanguage = By.xpath("//button[@class='MuiButtonBase-root MuiButton-root MuiButton-text MuiButton-textPrimary MuiButton-sizeMedium MuiButton-textSizeMedium MuiButton-disableElevation MuiButton-root MuiButton-text MuiButton-textPrimary MuiButton-sizeMedium MuiButton-textSizeMedium MuiButton-disableElevation css-j0p9p']");
    private By enLanguage = By.xpath("//li[normalize-space()='English']");
    @BeforeClass
    public void setupBrowser() {
        PropertiesFile.setPropertiesFile();
        driver = new BaseSetup().setupDriver(PropertiesFile.getPropValue("browser"));
    }
    @Test(priority = 1, description = "Create PlanType")
    //Login success
    public void CreatePlanType() throws InterruptedException, IOException {
        loginAdminPage = new LoginAdminPage(driver);
        validateHelper = new ValidateHelper(driver);
        driver.get(PropertiesFile.getPropValue("homeAdminUrl"));
        String expectedUrl = PropertiesFile.getPropValue("expectHomeAdminUrl");

        try {
            loginAdminPage.loginAdmin(PropertiesFile.getPropValue("emailAdmin"), PropertiesFile.getPropValue("passwordAdmin"));
            Thread.sleep(2000);
            String currentUrl = driver.getCurrentUrl();
            //Assert.assertTrue(currentUrl.equals(expectedUrl), "The login details are incorrect");
        } catch (Exception e) {
            System.out.println("Login failed due to exception: " + e.getMessage());
            Assert.fail("Login failed due to exception: " + e.getMessage());
        }
        Thread.sleep(2000);
        validateHelper.waitForPageLoaded();
        validateHelper.clickElementByJs(changLanguage);
        validateHelper.clickElementByJs(enLanguage);
        managePlanTypePage = new ManagePlanTypePage(driver);
        managePlanTypePage.createplantype("ANC","2", "12");
        Thread.sleep(5000);
    }
    @Test(priority = 2, description = "Hide PlanType")
    public void HidePlanType() throws InterruptedException, IOException {
        validateHelper.waitForPageLoaded();
        managePlanTypePage.hideplantype();
        Thread.sleep(2000);
    }
    @Test(priority = 3, description = "Statistic PlanType")
    public void StatisticPlanType() throws InterruptedException, IOException {
        validateHelper.waitForPageLoaded();
        managePlanTypePage.statisticplantype();
        Thread.sleep(2000);
    }
    @AfterClass
    public void closeBrowser() throws Exception {
        driver.quit();
    }
}
