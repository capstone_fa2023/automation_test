package anhbttester.testcase.user;

import anhbttester.base.BaseSetup;
import anhbttester.base.ValidateHelper;
import anhbttester.pages.UserPage.LoginPage;
import anhbttester.pages.UserPage.ChangePasswordPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utilities.PropertiesFile;

import java.util.ArrayList;

public class ChangePassTest {
    private WebDriver driver;
    private ValidateHelper validateHelper;
    private LoginPage loginPage;
    private ChangePasswordPage changePasswordPage;
    private By loginBtn = By.xpath("//span[@class='ButtonLogin encore-inverted-light-set']");
    private By avtImge = By.xpath("//img[@class='rounded-circle']");
    private By accountMenuBtn = By.xpath("//a[@href='/account/overview']");

    @BeforeClass
    public void setupBrowser() {
        PropertiesFile.setPropertiesFile();
        driver = new BaseSetup().setupDriver(PropertiesFile.getPropValue("browser"));
    }

    //@Test(priority = 1, description = "Login testcase ChangePass")

    @Test(priority = 1)
    //Open Account detail page
    public void OpenAccountDetail() throws InterruptedException {
        //////////Login
        loginPage = new LoginPage(driver);
        validateHelper = new ValidateHelper(driver);
        driver.get(PropertiesFile.getPropValue("homeUrl"));
        validateHelper.clickElement(loginBtn);

        String expectedUrl = PropertiesFile.getPropValue("expectHomeUrl");

        try {
            loginPage.login(PropertiesFile.getPropValue("email"), PropertiesFile.getPropValue("password"));
            Thread.sleep(2000);
            String currentUrl = driver.getCurrentUrl();
            Assert.assertTrue(currentUrl.contains(expectedUrl), "Login failed!");
        } catch (Exception e) {
            System.out.println("Login failed due to exception: " + e.getMessage());
            Assert.fail("Login failed due to exception: " + e.getMessage());
        }
        Thread.sleep(2000);
        /////////////////////
        validateHelper = new ValidateHelper(driver);
        validateHelper.clickElement(avtImge);
        validateHelper.clickElement(accountMenuBtn);
        Thread.sleep(2000);
    }

    @Test(priority = 2)
    public void ChangePassword() throws InterruptedException {
        validateHelper = new ValidateHelper(driver);
        ArrayList<String> tabs = new ArrayList<>(driver.getWindowHandles());
        driver.switchTo().window(tabs.get(1));
        validateHelper.waitForPageLoaded();
        changePasswordPage = new ChangePasswordPage(driver);
        changePasswordPage.openChangePass();
        changePasswordPage.changePassword(PropertiesFile.getPropValue("changePassCurrent"),PropertiesFile.getPropValue("changeNewPass"),PropertiesFile.getPropValue("changeRepeatPass"));
        Thread.sleep(2000);

        //Check success message
//        WebElement messageElement = driver.findElement(By.xpath("//div[@class='noti-box success']"));
//        String actualMessage = messageElement.getText();
//        String expectedMessage = "Change password successfully";
        //Assert.assertEquals(actualMessage, expectedMessage, "Change password not successful");
    }

    @AfterClass
    public void closeBrowser() {
        driver.quit();
    }
}
