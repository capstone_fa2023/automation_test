package anhbttester.testcase.user;

import anhbttester.base.BaseSetup;
import anhbttester.base.ValidateHelper;
import anhbttester.pages.UserPage.HomeCreateFolderPage;
import anhbttester.pages.UserPage.LoginPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utilities.PropertiesFile;

public class CreateFolderTest {
    private WebDriver driver;
    private ValidateHelper validateHelper;
    private LoginPage loginPage;
    private HomeCreateFolderPage homeCreateFolderPage;
    private By loginBtn = By.xpath("//span[@class='ButtonLogin encore-inverted-light-set']");
    @BeforeClass
    public void setupBrowser() {
        PropertiesFile.setPropertiesFile();
        driver = new BaseSetup().setupDriver(PropertiesFile.getPropValue("browser"));
    }

    @Test(priority = 1)
    //Create folder success
    public void CreateFolder() throws InterruptedException {
        ///////////////////Login
        loginPage = new LoginPage(driver);
        validateHelper = new ValidateHelper(driver);
        driver.get(PropertiesFile.getPropValue("homeUrl"));
        validateHelper.clickElement(loginBtn);

        String expectedUrl = PropertiesFile.getPropValue("expectHomeUrl");

        try {
            loginPage.login(PropertiesFile.getPropValue("email"), PropertiesFile.getPropValue("password"));
            Thread.sleep(2000);
            String currentUrl = driver.getCurrentUrl();
            Assert.assertTrue(currentUrl.contains(expectedUrl), "Login failed!");
        } catch (Exception e) {
            System.out.println("Login failed due to exception: " + e.getMessage());
            Assert.fail("Login failed due to exception: " + e.getMessage());
        }
        Thread.sleep(2000);
        //////////CreateFolder
        homeCreateFolderPage = new HomeCreateFolderPage(driver);
        int initialCount = homeCreateFolderPage.countFolder();
        homeCreateFolderPage.createFolder();
        Thread.sleep(8000);
        int newCount = homeCreateFolderPage.countFolder();
        Assert.assertEquals(newCount, initialCount + 1, "New folder count is not increased by 1");
        Thread.sleep(2000);
    }

    @Test(priority = 2, description = "Rename folder")
    public void RenameFolder() throws InterruptedException {
        homeCreateFolderPage.renamefolder(PropertiesFile.getPropValue("dataReNameFolder"));
        Thread.sleep(2000);
    }
    @Test(priority = 3, description = "Move folder")
    public void MoveFolder() throws InterruptedException {
        homeCreateFolderPage.movefolder();
        Thread.sleep(2000);
    }
    @Test(priority = 4, description = "Delete folder")
    public void DeleteFolder() throws InterruptedException {
        homeCreateFolderPage.deletefolder();
        Thread.sleep(2000);
    }

    @AfterClass
    public void closeBrowser() {
        driver.close();
    }
}
