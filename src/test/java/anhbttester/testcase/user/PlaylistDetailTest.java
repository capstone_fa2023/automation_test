package anhbttester.testcase.user;

import anhbttester.base.BaseSetup;
import anhbttester.base.ValidateHelper;
import anhbttester.pages.UserPage.AlbumDetailPage;
import anhbttester.pages.UserPage.LoginPage;
import anhbttester.pages.UserPage.PlaylistDetailPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utilities.PropertiesFile;

public class PlaylistDetailTest {
    private WebDriver driver;
    private ValidateHelper validateHelper;
    private LoginPage loginPage;
    private PlaylistDetailPage playlistDetailPage;
    private By loginBtn = By.xpath("//span[@class='ButtonLogin encore-inverted-light-set']");
    @BeforeClass
    public void setupBrowser() {
        PropertiesFile.setPropertiesFile();
        driver = new BaseSetup().setupDriver(PropertiesFile.getPropValue("browser"));
    }
    @Test(priority = 1, description = "View album detail")
    public void ViewAlbumDetail() throws InterruptedException {
        //Login
        loginPage = new LoginPage(driver);
        validateHelper = new ValidateHelper(driver);
        driver.get(PropertiesFile.getPropValue("homeUrl"));
        validateHelper.clickElement(loginBtn);

        String expectedUrl = PropertiesFile.getPropValue("expectHomeUrl");

        try {
            loginPage.login(PropertiesFile.getPropValue("email"), PropertiesFile.getPropValue("password"));
            validateHelper.waitForPageLoaded();
            String currentUrl = driver.getCurrentUrl();
            Assert.assertTrue(currentUrl.contains(expectedUrl), "Login failed!");
        } catch (Exception e) {
            System.out.println("Login failed due to exception: " + e.getMessage());
            Assert.fail("Login failed due to exception: " + e.getMessage());
        }
        Thread.sleep(2000);
        ///Search playlist
        playlistDetailPage = new PlaylistDetailPage(driver);
        playlistDetailPage.playlistdetail(PropertiesFile.getPropValue("dataSearchPlaylistDetail"));
        validateHelper.waitForPageLoaded();
        Thread.sleep(2000);
    }
    @Test(priority = 2, description = "Play playlist")
    public void PlayPlaylist() throws InterruptedException {
        validateHelper.waitForPageLoaded();
        playlistDetailPage.playplaylist();
        Thread.sleep(2000);
    }
    @Test(priority = 3, description = "Like playlist")
    public void LikePlaylist() throws InterruptedException {
        validateHelper.waitForPageLoaded();
        playlistDetailPage.likeplaylist();
        Thread.sleep(2000);
    }
    @Test(priority = 4, description = "Add to queue")
    public void AddQueuePlaylist() throws InterruptedException {
        validateHelper.waitForPageLoaded();
        playlistDetailPage.addqueue();
        Thread.sleep(2000);
    }
    @Test(priority = 5, description = "Remove from library")
    public void RemoveLibrary() throws InterruptedException {
        validateHelper.waitForPageLoaded();
        playlistDetailPage.removelibrary();
        Thread.sleep(2000);
    }
    @Test(priority = 6, description = "Add to folder")
    public void AddPlaylist() throws InterruptedException {
        validateHelper.waitForPageLoaded();
        playlistDetailPage.addfolder();
        Thread.sleep(2000);
    }
    @Test(priority = 7, description = "Share playlist")
    public void SharePlaylist() throws InterruptedException {
        validateHelper.waitForPageLoaded();
        playlistDetailPage.shareplaylist();
        Thread.sleep(2000);
    }
    @AfterClass
    public void tearDown() throws Exception {
        Thread.sleep(1000);
        driver.quit();
    }
}
