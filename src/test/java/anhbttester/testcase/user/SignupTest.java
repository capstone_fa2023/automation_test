package anhbttester.testcase.user;

import anhbttester.base.BaseSetup;
import anhbttester.base.ValidateHelper;
import anhbttester.pages.UserPage.SignupPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utilities.PropertiesFile;

public class SignupTest {
    private WebDriver driver;
    private ValidateHelper validateHelper;
    private SignupPage signupPage;
    private By signupBtn = By.xpath("//a[@class='ButtonWrapper Button-Download fs-6']");
    private By errorMessage = By.xpath("//div[@class='alert py-2']");

    @BeforeClass
    public void setupBrowser() {
        PropertiesFile.setPropertiesFile();
        driver = new BaseSetup().setupDriver(PropertiesFile.getPropValue("browser"));
        //driver = getDriver();
    }

    @Test
    //Signup success
    public void Signup() throws InterruptedException {
        signupPage = new SignupPage(driver);
        validateHelper = new ValidateHelper(driver);
        driver.get(PropertiesFile.getPropValue("homeUrl"));
        validateHelper.clickElement(signupBtn);
        String expectedUrl = PropertiesFile.getPropValue("expectHomeUrl");
        try {
            signupPage.signup(PropertiesFile.getPropValue("singupMail"), PropertiesFile.getPropValue("signupFname"), PropertiesFile.getPropValue("signupLname"), PropertiesFile.getPropValue("signupPass")
            , PropertiesFile.getPropValue("signupCfpass"), PropertiesFile.getPropValue("signupDay"), PropertiesFile.getPropValue("signupMonth"), PropertiesFile.getPropValue("signupYear"));
            Thread.sleep(2000);
            // Kiểm tra xem trang hiện tại sau khi đăng nhập có phải là trang chính hay không
            String currentUrl = driver.getCurrentUrl();
            Assert.assertTrue(currentUrl.contains(expectedUrl), "SignUp failed!");
            System.out.println("SignUp successful");
        } catch (Exception e) {
            // Xử lý lỗi đăng nhập ở đây
            System.out.println("SignUp failed due to exception: " + e.getMessage());
            Assert.fail("SignUp failed due to exception: " + e.getMessage());
        }

        Thread.sleep(2000);
    }

    @AfterClass
    public void closeBrowser() {
        driver.close();
    }
}
