package anhbttester.testcase.user;

import anhbttester.base.BaseSetup;
import anhbttester.base.ValidateHelper;
import anhbttester.pages.UserPage.LoginPage;
import anhbttester.pages.UserPage.RegisterArtistPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utilities.PropertiesFile;

public class RegisterArtistTest {
    private WebDriver driver;
    private ValidateHelper validateHelper;
    private LoginPage loginPage;
    private RegisterArtistPage registerArtistPage;
    private By loginBtn = By.xpath("//span[@class='ButtonLogin encore-inverted-light-set']");
    @BeforeClass
    public void setupBrowser() {
        PropertiesFile.setPropertiesFile();
        driver = new BaseSetup().setupDriver(PropertiesFile.getPropValue("browser"));
    }
    @Test(priority = 1)
    public void OpenFormRegister() throws InterruptedException {
        //////////Login
        loginPage = new LoginPage(driver);
        validateHelper = new ValidateHelper(driver);
        driver.get(PropertiesFile.getPropValue("homeUrl"));
        validateHelper.clickElement(loginBtn);

        String expectedUrl = PropertiesFile.getPropValue("expectHomeUrl");

        try {
            loginPage.login(PropertiesFile.getPropValue("email"), PropertiesFile.getPropValue("password"));
            Thread.sleep(2000);
            String currentUrl = driver.getCurrentUrl();
            Assert.assertTrue(currentUrl.contains(expectedUrl), "Login failed!");
        } catch (Exception e) {
            System.out.println("Login failed due to exception: " + e.getMessage());
            Assert.fail("Login failed due to exception: " + e.getMessage());
        }
        Thread.sleep(2000);
        /////////////////////
        registerArtistPage = new RegisterArtistPage(driver);
        registerArtistPage.openFormRegister();
        Thread.sleep(2000);
    }
    @Test(priority = 2, description = "Fill information to register form")
    public void FillRegister() throws InterruptedException {
        registerArtistPage.fillInforRegis(PropertiesFile.getPropValue("dataStageName"), PropertiesFile.getPropValue("dataBankcode"), PropertiesFile.getPropValue("dataAccountNum"), PropertiesFile.getPropValue("dataSocialLink"), PropertiesFile.getPropValue("dataDescription"));
        Thread.sleep(2000);
    }
    @AfterClass
    public void closeBrowser() {
        driver.quit();
    }
}
