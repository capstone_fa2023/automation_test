package anhbttester.testcase.user;

import anhbttester.base.BaseSetup;
import anhbttester.base.ValidateHelper;
import anhbttester.pages.UserPage.AlbumDetailPage;
import anhbttester.pages.UserPage.ArtistDetailPage;
import anhbttester.pages.UserPage.LoginPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utilities.PropertiesFile;

public class AlbumDetailTest {
    private WebDriver driver;
    private ValidateHelper validateHelper;
    private LoginPage loginPage;
    private AlbumDetailPage albumDetailPage;
    private By loginBtn = By.xpath("//span[@class='ButtonLogin encore-inverted-light-set']");
    @BeforeClass
    public void setupBrowser() {
        PropertiesFile.setPropertiesFile();
        driver = new BaseSetup().setupDriver(PropertiesFile.getPropValue("browser"));
    }
    @Test(priority = 1, description = "View album detail")
    public void ViewAlbumDetail() throws InterruptedException {
        //Login
        loginPage = new LoginPage(driver);
        validateHelper = new ValidateHelper(driver);
        driver.get(PropertiesFile.getPropValue("homeUrl"));
        validateHelper.clickElement(loginBtn);

        String expectedUrl = PropertiesFile.getPropValue("expectHomeUrl");

        try {
            loginPage.login(PropertiesFile.getPropValue("email"), PropertiesFile.getPropValue("password"));
            validateHelper.waitForPageLoaded();
            String currentUrl = driver.getCurrentUrl();
            Assert.assertTrue(currentUrl.contains(expectedUrl), "Login failed!");
        } catch (Exception e) {
            System.out.println("Login failed due to exception: " + e.getMessage());
            Assert.fail("Login failed due to exception: " + e.getMessage());
        }
        Thread.sleep(2000);
        ///Search album
        albumDetailPage = new AlbumDetailPage(driver);
        albumDetailPage.albumdetail(PropertiesFile.getPropValue("dataSearchAlbumDetail"));
        validateHelper.waitForPageLoaded();
        Thread.sleep(2000);
    }
    @Test(priority = 2, description = "Play album")
    public void PlayALbum() throws InterruptedException {
        validateHelper.waitForPageLoaded();
        albumDetailPage.playalbum();
        Thread.sleep(2000);
    }
    @Test(priority = 3, description = "Like album")
    public void LikeALbum() throws InterruptedException {
        validateHelper.waitForPageLoaded();
        albumDetailPage.likealbum();
        Thread.sleep(2000);
    }
    @Test(priority = 4, description = "Add to queue")
    public void AddQueueAlbum() throws InterruptedException {
        validateHelper.waitForPageLoaded();
        albumDetailPage.addqueue();
        Thread.sleep(2000);
    }
    @Test(priority = 5, description = "Remove from library")
    public void RemoveLibrary() throws InterruptedException {
        validateHelper.waitForPageLoaded();
        albumDetailPage.removelibrary();
        Thread.sleep(2000);
    }
    @Test(priority = 6, description = "Add to playlist")
    public void AddPlaylist() throws InterruptedException {
        validateHelper.waitForPageLoaded();
        albumDetailPage.addplaylist();
        Thread.sleep(2000);
    }
    @Test(priority = 7, description = "Share album")
    public void ShareAlbum() throws InterruptedException {
        validateHelper.waitForPageLoaded();
        albumDetailPage.sharealbum();
        Thread.sleep(2000);
    }
    @AfterClass
    public void tearDown() throws Exception {
        Thread.sleep(1000);
        driver.quit();
    }
}
