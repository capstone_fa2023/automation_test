package anhbttester.testcase.user;

import anhbttester.base.BaseSetup;
import anhbttester.base.ValidateHelper;
import anhbttester.pages.UserPage.ArtistDetailPage;
import anhbttester.pages.UserPage.LoginPage;
import anhbttester.pages.UserPage.TrackDetailPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utilities.PropertiesFile;

public class TrackDetailTest {
    private WebDriver driver;
    private ValidateHelper validateHelper;
    private LoginPage loginPage;
    private TrackDetailPage trackDetailPage;
    private By loginBtn = By.xpath("//span[@class='ButtonLogin encore-inverted-light-set']");
    @BeforeClass
    public void setupBrowser() {
        PropertiesFile.setPropertiesFile();
        driver = new BaseSetup().setupDriver(PropertiesFile.getPropValue("browser"));
    }
    @Test(priority = 1, description = "View artist detail")
    //Search track
    public void ViewArtistDetail() throws InterruptedException {
        //Login
        loginPage = new LoginPage(driver);
        validateHelper = new ValidateHelper(driver);
        driver.get(PropertiesFile.getPropValue("homeUrl"));
        validateHelper.clickElement(loginBtn);

        String expectedUrl = PropertiesFile.getPropValue("expectHomeUrl");

        try {
            loginPage.login(PropertiesFile.getPropValue("email"), PropertiesFile.getPropValue("password"));
            validateHelper.waitForPageLoaded();
            String currentUrl = driver.getCurrentUrl();
            Assert.assertTrue(currentUrl.contains(expectedUrl), "Login failed!");
        } catch (Exception e) {
            System.out.println("Login failed due to exception: " + e.getMessage());
            Assert.fail("Login failed due to exception: " + e.getMessage());
        }
        Thread.sleep(2000);
        ///View track detail
        trackDetailPage = new TrackDetailPage(driver);
        trackDetailPage.trackdetail(PropertiesFile.getPropValue("dataSearchTrackDetail"));
        validateHelper.waitForPageLoaded();
        Thread.sleep(2000);
    }
    @Test(priority = 2, description = "Play track")
    //Play track
    public void PlayTrack() throws InterruptedException {
        validateHelper.waitForPageLoaded();
        trackDetailPage.playtrack();
        Thread.sleep(2000);
    }
    @Test(priority = 3, description = "Like track")
    //Like track
    public void LikeTrack() throws InterruptedException {
        validateHelper.waitForPageLoaded();
        trackDetailPage.liketrack();
        Thread.sleep(4000);
    }
    @Test(priority = 4, description = "Add queue track")
    //Queue track
    public void QueueTrack() throws InterruptedException {
        validateHelper.waitForPageLoaded();
        trackDetailPage.addqueue();
        Thread.sleep(4000);
    }
    @Test(priority = 5, description = "Remove track from library")
    //Remove from playlist
    public void RemoveLibrary() throws InterruptedException {
        validateHelper.waitForPageLoaded();
        trackDetailPage.removelibrary();
        Thread.sleep(2000);
    }
    @Test(priority = 6, description = "Add playlist")
    //Add playlist
    public void AddPlaylist() throws InterruptedException {
        validateHelper.waitForPageLoaded();
        trackDetailPage.addplaylist();
        Thread.sleep(2000);
    }
    @Test(priority = 7, description = "Copy link track")
    //Copy link track
    public void SharePlaylist() throws InterruptedException {
        validateHelper.waitForPageLoaded();
        trackDetailPage.copylink();
        Thread.sleep(2000);
    }
    @Test(priority = 8, description = "Comment Track")
    //Comment track
    public void CommentTrack() throws InterruptedException {
        validateHelper.waitForPageLoaded();
        trackDetailPage.commentTrack(PropertiesFile.getPropValue("dataComment"));
        Thread.sleep(2000);
    }
        @AfterClass
        public void tearDown() throws Exception {
            Thread.sleep(1000);
            driver.quit();
        }
}
