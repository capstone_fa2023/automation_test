package anhbttester.testcase.user;

import anhbttester.base.BaseSetup;
import anhbttester.base.ValidateHelper;
import anhbttester.pages.UserPage.SearchTrackPage;
import anhbttester.pages.UserPage.LoginPage;
//import anhqa.com.common.helpers.CaptureHelpers;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.*;
import utilities.PropertiesFile;


public class SearchTest {
    private WebDriver driver;
    private ValidateHelper validateHelper;
    private LoginPage loginPage;
    private SearchTrackPage searchTrackPage;
    private By loginBtn = By.xpath("//span[@class='ButtonLogin encore-inverted-light-set']");

    @BeforeClass
    public void setupBrowser() {
        PropertiesFile.setPropertiesFile();
        driver = new BaseSetup().setupDriver(PropertiesFile.getPropValue("browser"));
    }

    @Test(priority = 1, description = "Process search track")
    //Search track
    public void SearchTrack() throws InterruptedException {
        //Login
        loginPage = new LoginPage(driver);
        validateHelper = new ValidateHelper(driver);
        driver.get(PropertiesFile.getPropValue("homeUrl"));
        validateHelper.clickElement(loginBtn);

        String expectedUrl = PropertiesFile.getPropValue("expectHomeUrl");

        try {
            loginPage.login(PropertiesFile.getPropValue("email"), PropertiesFile.getPropValue("password"));
            validateHelper.waitForPageLoaded();
            String currentUrl = driver.getCurrentUrl();
            Assert.assertTrue(currentUrl.contains(expectedUrl), "Login failed!");
        } catch (Exception e) {
            System.out.println("Login failed due to exception: " + e.getMessage());
            Assert.fail("Login failed due to exception: " + e.getMessage());
        }
        Thread.sleep(2000);

        ///Search Track
        searchTrackPage = new SearchTrackPage(driver);
        searchTrackPage.searchTrack(PropertiesFile.getPropValue("dataSearch"));
        validateHelper.waitForPageLoaded();
        //Check Element
        WebElement searchPage = driver.findElement(By.xpath("//div[@id='searchPage']"));
        String searchPageText = searchPage.getText();
        Assert.assertTrue(searchPageText.contains(PropertiesFile.getPropValue("dataSearch")), "Search failed!");
        Thread.sleep(2000);
    }
    @Test(priority = 2, description = "Search Artist")
    //Search track
    public void SearchArtists() throws InterruptedException {
        validateHelper.waitForPageLoaded();
        searchTrackPage.searchartist();
        Thread.sleep(2000);
    }
    @Test(priority = 3, description = "Search Track")
    //Search track
    public void SearchTracks() throws InterruptedException {
        validateHelper.waitForPageLoaded();
        searchTrackPage.searchtrack();
        Thread.sleep(2000);
    }
    @Test(priority = 4, description = "Search Album")
    //Search track
    public void SearchAlbums() throws InterruptedException {
        validateHelper.waitForPageLoaded();
        searchTrackPage.searchalbum();
        Thread.sleep(2000);
    }
    @Test(priority = 5, description = "Search Playlist")
    //Search track
    public void SearchPlaylists() throws InterruptedException {
        validateHelper.waitForPageLoaded();
        searchTrackPage.searchplaylist();
        Thread.sleep(2000);
    }
    @AfterClass
    public void tearDown() throws Exception {
        Thread.sleep(1000);
        driver.quit();
    }

}
