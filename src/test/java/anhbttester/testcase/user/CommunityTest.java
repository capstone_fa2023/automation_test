package anhbttester.testcase.user;

import anhbttester.base.BaseSetup;
import anhbttester.base.ValidateHelper;
import anhbttester.pages.UserPage.CommunityPage;
import anhbttester.pages.UserPage.LoginPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utilities.PropertiesFile;

public class CommunityTest {
    private WebDriver driver;
    private ValidateHelper validateHelper;
    private LoginPage loginPage;
    private CommunityPage communityPage;
    private By loginBtn = By.xpath("//span[@class='ButtonLogin encore-inverted-light-set']");
    @BeforeClass
    public void setupBrowser() {
        PropertiesFile.setPropertiesFile();
        driver = new BaseSetup().setupDriver(PropertiesFile.getPropValue("browser"));
    }
    @Test(priority = 1, description = "Process search album community")
    //Search track
    public void SearchAlbumCommunity() throws InterruptedException {
        //Login
        loginPage = new LoginPage(driver);
        validateHelper = new ValidateHelper(driver);
        driver.get(PropertiesFile.getPropValue("homeUrl"));
        validateHelper.clickElement(loginBtn);

        String expectedUrl = PropertiesFile.getPropValue("expectHomeUrl");

        try {
            loginPage.login(PropertiesFile.getPropValue("email"), PropertiesFile.getPropValue("password"));
            validateHelper.waitForPageLoaded();
            String currentUrl = driver.getCurrentUrl();
            //Assert.assertTrue(currentUrl.contains(expectedUrl), "Login failed!");
        } catch (Exception e) {
            System.out.println("Login failed due to exception: " + e.getMessage());
            Assert.fail("Login failed due to exception: " + e.getMessage());
        }
        Thread.sleep(2000);
        communityPage = new CommunityPage(driver);
        communityPage.searchAlbumCommunity(PropertiesFile.getPropValue("dataSearchCommunity"));
        validateHelper.waitForPageLoaded();
        Thread.sleep(2000);
    }
    @Test(priority = 2, description = "Process search track community")
    public void SearchTrackCommunity() throws InterruptedException {
        validateHelper.waitForPageLoaded();
        communityPage.searchTrackCommunity();
        Thread.sleep(2000);
    }
    @Test(priority = 3, description = "Process search playlist community")
    public void SearchPlaylistCommunity() throws InterruptedException {
        validateHelper.waitForPageLoaded();
        communityPage.searchPlaylistCommunity();
        Thread.sleep(2000);
    }
    @Test(priority = 4, description = "Process search artist community")
    public void SearchArtistCommunity() throws InterruptedException {
        validateHelper.waitForPageLoaded();
        communityPage.searchArtistCommunity();
        Thread.sleep(2000);
    }
    @Test(priority = 5, description = "Process order date community")
    public void OrderDateNewCommunity() throws InterruptedException {
        validateHelper.waitForPageLoaded();
        communityPage.selectDateNewCommunity();
        Thread.sleep(2000);
    }
    @Test(priority = 6, description = "Process order date community")
    public void OrderDateOldCommunity() throws InterruptedException {
        validateHelper.waitForPageLoaded();
        communityPage.selectDateOldCommunity();
        Thread.sleep(2000);
    }
    @Test(priority = 7, description = "Process choose low listens community")
    public void HighListensCommunity() throws InterruptedException {
        validateHelper.waitForPageLoaded();
        communityPage.selectHighListenCommunity();
        Thread.sleep(2000);
    }
    @Test(priority = 8, description = "Process choose high listens community")
    public void LowListensCommunity() throws InterruptedException {
        validateHelper.waitForPageLoaded();
        communityPage.selectLowListenCommunity();
        Thread.sleep(2000);
    }
    @Test(priority = 9, description = "Process choose low like community")
    //Search track
    public void HighLikesCommunity() throws InterruptedException {
        validateHelper.waitForPageLoaded();
        communityPage.selectHighLikeCommunity();
        Thread.sleep(2000);
    }
    @Test(priority = 10, description = "Process choose high like community")
    //Search track
    public void LowLikesCommunity() throws InterruptedException {
        validateHelper.waitForPageLoaded();
        communityPage.selectLowLikeCommunity();
        Thread.sleep(2000);
    }
    @AfterClass
    public void tearDown() throws Exception {
        Thread.sleep(1000);
        driver.quit();
    }
}
