package anhbttester.testcase.user;

import anhbttester.base.BaseSetup;
import anhbttester.base.ValidateHelper;
import anhbttester.pages.UserPage.LoginPage;
//import anhqa.com.common.helpers.CaptureHelpers;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.*;
import utilities.PropertiesFile;

import java.io.IOException;


public class LoginTest{
    private WebDriver driver;
    private ValidateHelper validateHelper;
    private LoginPage loginPage;
    private By loginBtn = By.xpath("//span[@class='ButtonLogin encore-inverted-light-set']");

    @BeforeClass
    public void setupBrowser() throws Exception {
        PropertiesFile.setPropertiesFile();
        driver = new BaseSetup().setupDriver(PropertiesFile.getPropValue("browser"));
    }

    @Test
    //Login success
    public void Login() throws InterruptedException, IOException {
        loginPage = new LoginPage(driver);
        validateHelper = new ValidateHelper(driver);
        driver.get(PropertiesFile.getPropValue("homeUrl"));
        validateHelper.clickElement(loginBtn);

        String expectedUrl = PropertiesFile.getPropValue("expectHomeUrl");

        try {
            loginPage.login(PropertiesFile.getPropValue("email"), PropertiesFile.getPropValue("password"));
            Thread.sleep(2000);
            String currentUrl = driver.getCurrentUrl();
            //Assert.assertTrue(currentUrl.equals(expectedUrl), "The login details are incorrect");
        } catch (Exception e) {
            System.out.println("Login failed due to exception: " + e.getMessage());
            Assert.fail("Login failed due to exception: " + e.getMessage());
        }
        Thread.sleep(2000);
    }

    @AfterClass
    public void closeBrowser() throws Exception {
        driver.quit();
        //CaptureHelpers.stopRecord();
    }
}
