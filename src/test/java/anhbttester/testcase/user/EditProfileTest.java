package anhbttester.testcase.user;

import anhbttester.base.BaseSetup;
import anhbttester.base.ValidateHelper;
import anhbttester.pages.UserPage.LoginPage;
import anhbttester.pages.UserPage.AccountOverviewPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utilities.PropertiesFile;

import java.util.ArrayList;

public class EditProfileTest {
    private WebDriver driver;
    private ValidateHelper validateHelper;
    private LoginPage loginPage;
    private AccountOverviewPage accountOverviewPage;
    private By loginBtn = By.xpath("//span[@class='ButtonLogin encore-inverted-light-set']");
    private By avtImge = By.xpath("//img[@class='rounded-circle']");
    private By accountMenuBtn = By.xpath("//a[@href='/account/overview']");

    @BeforeClass
    public void setupBrowser() {
        PropertiesFile.setPropertiesFile();
        driver = new BaseSetup().setupDriver(PropertiesFile.getPropValue("browser"));
        //driver = getDriver();
    }

    //@Test(priority = 1, description = "Login system")
    @Test(priority = 1)
    public void OpenAccountDetail() throws InterruptedException {
        //Login
        loginPage = new LoginPage(driver);
        validateHelper = new ValidateHelper(driver);
        driver.get(PropertiesFile.getPropValue("homeUrl"));
        validateHelper.clickElement(loginBtn);

        String expectedUrl = PropertiesFile.getPropValue("expectHomeUrl");

        try {
            loginPage.login(PropertiesFile.getPropValue("email"), PropertiesFile.getPropValue("password"));
            Thread.sleep(2000);
            String currentUrl = driver.getCurrentUrl();
            Assert.assertTrue(currentUrl.contains(expectedUrl), "Login failed!");
        } catch (Exception e) {
            System.out.println("Login failed due to exception: " + e.getMessage());
            Assert.fail("Login failed due to exception: " + e.getMessage());
        }
        Thread.sleep(2000);
        //Open Account detail page
        validateHelper = new ValidateHelper(driver);
        validateHelper.clickElement(avtImge);
        validateHelper.clickElement(accountMenuBtn);
        Thread.sleep(2000);
    }

    @Test(priority = 2)
    public void EditProfile() throws InterruptedException {

        ArrayList<String> tabs = new ArrayList<>(driver.getWindowHandles());
        driver.switchTo().window(tabs.get(1));
        accountOverviewPage = new AccountOverviewPage(driver);
        accountOverviewPage.openEditProfile();
        accountOverviewPage.editProfile(PropertiesFile.getPropValue("editFname"), PropertiesFile.getPropValue("editLname"));
        Thread.sleep(2000);

        //Check success message
        WebElement messageElement = driver.findElement(By.xpath("//div[@class='noti-box success']"));
        String actualMessage = messageElement.getText();
        String expectedMessage = "Profile updated successfully!";
        Assert.assertEquals(actualMessage, expectedMessage, "Edit profile not successful");
    }

    @AfterClass
    public void closeBrowser() {
        driver.quit();
    }
}
