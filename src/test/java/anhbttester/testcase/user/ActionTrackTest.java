package anhbttester.testcase.user;

import anhbttester.base.BaseSetup;
import anhbttester.base.ValidateHelper;
import anhbttester.pages.UserPage.ActionTrackPage;
import anhbttester.pages.UserPage.LoginPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utilities.PropertiesFile;

public class ActionTrackTest {
    private WebDriver driver;
    private ValidateHelper validateHelper;
    private LoginPage loginPage;
    private ActionTrackPage actionTrackPage;
    private By loginBtn = By.xpath("//span[@class='ButtonLogin encore-inverted-light-set']");
    @BeforeClass
    public void setupBrowser() {
        PropertiesFile.setPropertiesFile();
        driver = new BaseSetup().setupDriver(PropertiesFile.getPropValue("browser"));
        //driver = getDriver();
    }

    @Test(priority = 1, description = "Process search track")
    //Search track
    public void SearchTrack() throws InterruptedException {
        ///Login
        loginPage = new LoginPage(driver);
        validateHelper = new ValidateHelper(driver);
        driver.get(PropertiesFile.getPropValue("homeUrl"));
        validateHelper.clickElement(loginBtn);

        String expectedUrl = PropertiesFile.getPropValue("expectHomeUrl");

        try {
            loginPage.login(PropertiesFile.getPropValue("email"), PropertiesFile.getPropValue("password"));
            Thread.sleep(2000);
            String currentUrl = driver.getCurrentUrl();
            Assert.assertTrue(currentUrl.contains(expectedUrl), "Login failed!");
        } catch (Exception e) {
            System.out.println("Login failed due to exception: " + e.getMessage());
            Assert.fail("Login failed due to exception: " + e.getMessage());
        }
        Thread.sleep(2000);
        ////Search track
        actionTrackPage = new ActionTrackPage(driver);
        actionTrackPage.searchTrack(PropertiesFile.getPropValue("dataSearch"));
        validateHelper.waitForPageLoaded();
        Thread.sleep(2000);
    }

    @Test(priority = 2, description = "Play track")
    //Play track
    public void PlayTrack() throws InterruptedException {
        validateHelper.waitForPageLoaded();
        actionTrackPage.playtrack();
        Thread.sleep(4000);
    }

    @Test(priority = 3, description = "Pause track")
    //Pause track
    public void PauseTrack() throws InterruptedException {
        //actionTrackPage = new ActionTrackPage(driver);
        validateHelper.waitForPageLoaded();
        actionTrackPage.pausetrack();
        Thread.sleep(4000);
    }

    @Test(priority = 4, description = "Next track")
    //Next track
    public void NextTrack() throws InterruptedException {
        //actionTrackPage = new ActionTrackPage(driver);
        validateHelper.waitForPageLoaded();
        actionTrackPage.nexttrack();
        Thread.sleep(4000);
    }

    @Test(priority = 5, description = "Previous track")
    //Next track
    public void PreviousTrack() throws InterruptedException {
        validateHelper.waitForPageLoaded();
        actionTrackPage.previoustrack();
        Thread.sleep(4000);
    }
    @Test(priority = 6, description = "Shuffle track")
    //Shuffle track
    public void ShuffleTrack() throws InterruptedException {
        validateHelper.waitForPageLoaded();
        actionTrackPage.shuffletrack();
        Thread.sleep(4000);
    }
    @Test(priority = 7, description = "Loop track")
    //Loop track
    public void LoopTrack() throws InterruptedException {
        validateHelper.waitForPageLoaded();
        actionTrackPage.looptrack();
        Thread.sleep(4000);
    }
    @Test(priority = 8, description = "Open lyrics")
    public void OpenLyrics() throws InterruptedException {
        validateHelper.waitForPageLoaded();
        actionTrackPage.openLyrics();
        Thread.sleep(4000);
    }
    @AfterClass
    public void tearDown() throws Exception {
        Thread.sleep(2000);
        driver.quit();
    }
}
