package anhbttester.testcase.user;

import anhbttester.base.BaseSetup;
import anhbttester.base.ValidateHelper;
import anhbttester.pages.UserPage.ArtistDetailPage;
import anhbttester.pages.UserPage.LoginPage;
import anhbttester.pages.UserPage.SearchTrackPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utilities.PropertiesFile;

import java.util.ArrayList;

public class ArtistDetailTest {
    private WebDriver driver;
    private ValidateHelper validateHelper;
    private LoginPage loginPage;
    private ArtistDetailPage artistDetailPage;
    private By loginBtn = By.xpath("//span[@class='ButtonLogin encore-inverted-light-set']");
    @BeforeClass
    public void setupBrowser() {
        PropertiesFile.setPropertiesFile();
        driver = new BaseSetup().setupDriver(PropertiesFile.getPropValue("browser"));
    }
    @Test(priority = 1, description = "View artist detail")
    //Search track
    public void ViewArtistDetail() throws InterruptedException {
        //Login
        loginPage = new LoginPage(driver);
        validateHelper = new ValidateHelper(driver);
        driver.get(PropertiesFile.getPropValue("homeUrl"));
        validateHelper.clickElement(loginBtn);

        String expectedUrl = PropertiesFile.getPropValue("expectHomeUrl");

        try {
            loginPage.login(PropertiesFile.getPropValue("email"), PropertiesFile.getPropValue("password"));
            validateHelper.waitForPageLoaded();
            String currentUrl = driver.getCurrentUrl();
            Assert.assertTrue(currentUrl.contains(expectedUrl), "Login failed!");
        } catch (Exception e) {
            System.out.println("Login failed due to exception: " + e.getMessage());
            Assert.fail("Login failed due to exception: " + e.getMessage());
        }
        Thread.sleep(2000);
        ///Search artist
        artistDetailPage = new ArtistDetailPage(driver);
        artistDetailPage.artistdetail(PropertiesFile.getPropValue("dataSearchArtistDetail"));
        validateHelper.waitForPageLoaded();
        Thread.sleep(2000);
    }
    @Test(priority = 2, description = "Play in Artist detail")
    //Play track
    public void PlayArtistDetail() throws InterruptedException {
        validateHelper.waitForPageLoaded();
        artistDetailPage.playinartistdetail();
        Thread.sleep(5000);
    }
    @Test(priority = 3, description = "Follow artist")
    //Follow track
    public void FollowArtist() throws InterruptedException {
        validateHelper.waitForPageLoaded();
        artistDetailPage.followArtist();
        Thread.sleep(5000);
    }
    @Test(priority = 4, description = "Overview artist")
    //Overview artist
    public void OverviewArtist() throws InterruptedException {
        validateHelper.waitForPageLoaded();
        artistDetailPage.overviewArtist();
        Thread.sleep(5000);
    }
    @Test(priority = 5, description = "Comment post")
    //Comment post
    public void CommentPost() throws InterruptedException {
        validateHelper.waitForPageLoaded();
        artistDetailPage.commentPost(PropertiesFile.getPropValue("dataComment"));
        Thread.sleep(4000);
    }

    @Test(priority = 6, description = "Unfollow artist")
    public void UnfollowArtist() throws InterruptedException {
        validateHelper.waitForPageLoaded();
        artistDetailPage.unfollow();
        Thread.sleep(2000);
    }
    @Test(priority = 7, description = "Copy link artist")
    public void ShareArtist() throws InterruptedException {
        validateHelper.waitForPageLoaded();
        artistDetailPage.copylinkartist();
        Thread.sleep(2000);
    }
    @Test(priority = 8, description = "Open report photo page")
    public void OpenReportPhoto() throws InterruptedException {
        validateHelper.waitForPageLoaded();
        artistDetailPage.openreport();
        Thread.sleep(2000);
    }
    @Test(priority = 9, description = "Report photo")
    public void ProcessReportPhoto() throws InterruptedException {
        validateHelper = new ValidateHelper(driver);
        ArrayList<String> tabs = new ArrayList<>(driver.getWindowHandles());
        driver.switchTo().window(tabs.get(1));
        validateHelper.waitForPageLoaded();
        artistDetailPage = new ArtistDetailPage(driver);
        artistDetailPage.processreportphoto(PropertiesFile.getPropValue("dataReportPhoto"));
        Thread.sleep(2000);
    }

//    @Test(priority = 12, description = "Report image")
//    public void ProcessReportImage() throws InterruptedException {
//        validateHelper = new ValidateHelper(driver);
//        ArrayList<String> tabs = new ArrayList<>(driver.getWindowHandles());
//        driver.switchTo().window(tabs.get(1));
//        validateHelper.waitForPageLoaded();
//        artistDetailPage = new ArtistDetailPage(driver);
//        artistDetailPage.processreportimage(PropertiesFile.getPropValue("dataReportImage"));
//        Thread.sleep(2000);
//    }
//
//    @Test(priority = 15, description = "User report")
//    public void ProcessUserReport() throws InterruptedException {
//        validateHelper = new ValidateHelper(driver);
//        ArrayList<String> tabs = new ArrayList<>(driver.getWindowHandles());
//        driver.switchTo().window(tabs.get(1));
//        validateHelper.waitForPageLoaded();
//        artistDetailPage = new ArtistDetailPage(driver);
//        artistDetailPage.processuserrp(PropertiesFile.getPropValue("dataUserReport"));
//        Thread.sleep(2000);
//    }
    @AfterClass
    public void tearDown() throws Exception {
        Thread.sleep(1000);
        driver.quit();
    }
}
