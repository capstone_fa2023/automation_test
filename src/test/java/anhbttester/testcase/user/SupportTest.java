package anhbttester.testcase.user;

import anhbttester.base.BaseSetup;
import anhbttester.base.ValidateHelper;
import anhbttester.pages.UserPage.AboutPage;
import anhbttester.pages.UserPage.LoginPage;
import anhbttester.pages.UserPage.SupportPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utilities.PropertiesFile;

import java.io.IOException;
import java.util.ArrayList;

public class SupportTest {
    private WebDriver driver;
    private ValidateHelper validateHelper;
    private LoginPage loginPage;
    private SupportPage supportPage;
    private By loginBtn = By.xpath("//span[@class='ButtonLogin encore-inverted-light-set']");
    @BeforeClass
    public void setupBrowser() throws Exception {
        PropertiesFile.setPropertiesFile();
        driver = new BaseSetup().setupDriver(PropertiesFile.getPropValue("browser"));
    }
    @Test(priority = 1)
    public void SupportPageTest() throws InterruptedException, IOException {
        loginPage = new LoginPage(driver);
        validateHelper = new ValidateHelper(driver);
        driver.get(PropertiesFile.getPropValue("homeUrl"));
        validateHelper.clickElement(loginBtn);

        String expectedUrl = PropertiesFile.getPropValue("expectHomeUrl");

        try {
            loginPage.login(PropertiesFile.getPropValue("email"), PropertiesFile.getPropValue("password"));
            Thread.sleep(2000);
            String currentUrl = driver.getCurrentUrl();
            //Assert.assertTrue(currentUrl.equals(expectedUrl), "The login details are incorrect");
        } catch (Exception e) {
            System.out.println("Login failed due to exception: " + e.getMessage());
            Assert.fail("Login failed due to exception: " + e.getMessage());
        }
        Thread.sleep(2000);
        supportPage = new SupportPage(driver);
        supportPage.opensupportpage();
        Thread.sleep(2000);
    }

    @Test(priority = 2, description = "Support change password")
    public void SupportChangePass() throws InterruptedException, IOException {
        ArrayList<String> tabs = new ArrayList<>(driver.getWindowHandles());
        driver.switchTo().window(tabs.get(1));
        supportPage.opensupportforpassword();
        Thread.sleep(2000);
    }
    @Test(priority = 3, description = "Support forgot Infor")
    public void SupportForgotInfor() throws InterruptedException, IOException {
        supportPage.opensupportforinfor();
        Thread.sleep(2000);
    }
    @Test(priority = 4, description = "Support login facebook")
    public void SupportLoginFB() throws InterruptedException, IOException {
        supportPage.opensupportforloginfb();
        Thread.sleep(2000);
    }
    @Test(priority = 5, description = "Support login google")
    public void SupportLoginGG() throws InterruptedException, IOException {
        supportPage.opensupportforlogingg();
        Thread.sleep(2000);
    }
    @Test(priority = 6, description = "Support account disable")
    public void SupportAccountDisable() throws InterruptedException, IOException {
        supportPage.opensupportfordisable();
        Thread.sleep(2000);
    }
    @AfterClass
    public void closeBrowser() throws Exception {
        driver.quit();
    }
}
