package anhbttester.testcase.user;

import anhbttester.base.BaseSetup;
import anhbttester.base.ValidateHelper;
import anhbttester.pages.UserPage.HomeCreatePlaylistPage;
import anhbttester.pages.UserPage.LoginPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utilities.PropertiesFile;

public class CreatePlaylistTest {
    private WebDriver driver;
    private ValidateHelper validateHelper;
    private LoginPage loginPage;
    private HomeCreatePlaylistPage homeCreatePlaylist;
    private By loginBtn = By.xpath("//span[@class='ButtonLogin encore-inverted-light-set']");
    @BeforeClass
    public void setupBrowser() {
        PropertiesFile.setPropertiesFile();
        driver = new BaseSetup().setupDriver(PropertiesFile.getPropValue("browser"));
        //driver = getDriver();
    }

    //@Test(priority = 1, description = "Login testcase CreatePlaylist")
    @Test(priority = 1, description = "Create Playlist")
    //Create playlist success
    public void CreatePlaylist() throws InterruptedException {
        //Login
        loginPage = new LoginPage(driver);
        validateHelper = new ValidateHelper(driver);
        driver.get(PropertiesFile.getPropValue("homeUrl"));
        validateHelper.clickElement(loginBtn);

        String expectedUrl = PropertiesFile.getPropValue("expectHomeUrl");

        try {
            loginPage.login(PropertiesFile.getPropValue("email"), PropertiesFile.getPropValue("password"));
            Thread.sleep(2000);
            String currentUrl = driver.getCurrentUrl();
            Assert.assertTrue(currentUrl.contains(expectedUrl), "Login failed!");
        } catch (Exception e) {
            System.out.println("Login failed due to exception: " + e.getMessage());
            Assert.fail("Login failed due to exception: " + e.getMessage());
        }
        Thread.sleep(2000);

        //Create Playlist
        homeCreatePlaylist = new HomeCreatePlaylistPage(driver);
        int initialCount = homeCreatePlaylist.countPlaylists();
        homeCreatePlaylist.createPlaylist();
        Thread.sleep(8000);
        int newCount = homeCreatePlaylist.countPlaylists();
        Assert.assertEquals(newCount, initialCount + 1, "New playlist count is not increased by 1");
        //Assert.assertTrue(homeCreatePlaylist.checkPlaylistDisplay("My Playlist #18"),"Playlist not display");
        Thread.sleep(2000);
    }
    @Test(priority = 2, description = "Add queue Playlist")
    public void PlaylistAddQueue() throws InterruptedException {
        homeCreatePlaylist.addtoqueue();
        Thread.sleep(2000);
    }
    @Test(priority = 3, description = "Add to profile")
    public void AddProfile() throws InterruptedException {
        homeCreatePlaylist.addtoprofile();
        Thread.sleep(2000);
    }
    @Test(priority = 4, description = "Pin Playlist")
    public void PinPlaylist() throws InterruptedException {
        homeCreatePlaylist.pinplaylist();
        Thread.sleep(2000);
    }
    @Test(priority = 5, description = "Edit Playlist")
    public void EditPlaylist() throws InterruptedException {
        homeCreatePlaylist.editplaylist();
        Thread.sleep(2000);
    }
    @Test(priority = 6, description = "Move Playlist")
    public void MovePlaylist() throws InterruptedException {
        homeCreatePlaylist.movefolder();
        Thread.sleep(2000);
    }
    @Test(priority = 7, description = "Delete Playlist")
    public void DeletePlaylist() throws InterruptedException {
        homeCreatePlaylist.deleteplaylist();
        Thread.sleep(2000);
    }
    @AfterClass
    public void closeBrowser() {
        driver.close();
    }
}
