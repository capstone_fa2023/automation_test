package anhbttester.testcase.user;

import anhbttester.base.BaseSetup;
import anhbttester.base.ValidateHelper;
import anhbttester.pages.UserPage.ArtistDetailPage;
import anhbttester.pages.UserPage.LoginPage;
import anhbttester.pages.UserPage.NotificationPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utilities.PropertiesFile;

public class NotificationTest {
    private WebDriver driver;
    private ValidateHelper validateHelper;
    private LoginPage loginPage;
    private ArtistDetailPage artistDetailPage;
    private NotificationPage notificationPage;
    private By loginBtn = By.xpath("//span[@class='ButtonLogin encore-inverted-light-set']");
    @BeforeClass
    public void setupBrowser() {
        PropertiesFile.setPropertiesFile();
        driver = new BaseSetup().setupDriver(PropertiesFile.getPropValue("browser"));
    }
    @Test(priority = 1, description = "View artist detail")
    //Search track
    public void ViewArtistDetail() throws InterruptedException {
        //Login
        loginPage = new LoginPage(driver);
        validateHelper = new ValidateHelper(driver);
        driver.get(PropertiesFile.getPropValue("homeUrl"));
        validateHelper.clickElement(loginBtn);

        String expectedUrl = PropertiesFile.getPropValue("expectHomeUrl");

        try {
            loginPage.login(PropertiesFile.getPropValue("email"), PropertiesFile.getPropValue("password"));
            validateHelper.waitForPageLoaded();
            String currentUrl = driver.getCurrentUrl();
            Assert.assertTrue(currentUrl.contains(expectedUrl), "Login failed!");
        } catch (Exception e) {
            System.out.println("Login failed due to exception: " + e.getMessage());
            Assert.fail("Login failed due to exception: " + e.getMessage());
        }
        Thread.sleep(2000);
        ///Search artist
        artistDetailPage = new ArtistDetailPage(driver);
        artistDetailPage.artistdetail(PropertiesFile.getPropValue("dataSearchArtistDetail"));
        validateHelper.waitForPageLoaded();
        Thread.sleep(2000);
    }
    @Test(priority = 2, description = "Follow artist")
    //Follow track
    public void FollowArtist() throws InterruptedException {
        validateHelper.waitForPageLoaded();
        artistDetailPage.followArtist();
        Thread.sleep(5000);
    }
    @Test(priority = 3, description = "View notification")
    //Follow track
    public void ViewNotification() throws InterruptedException {
        validateHelper.waitForPageLoaded();
        notificationPage = new NotificationPage(driver);
        notificationPage.viewnotification();
        Thread.sleep(5000);
    }
    @AfterClass
    public void closeBrowser() {
        driver.close();
    }
}
