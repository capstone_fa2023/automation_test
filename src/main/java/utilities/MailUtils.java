package utilities;

import jakarta.mail.*;
import jakarta.mail.internet.InternetAddress;
import jakarta.mail.internet.MimeBodyPart;
import jakarta.mail.internet.MimeMessage;
import jakarta.mail.internet.MimeMultipart;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

public class MailUtils {
    private String username;
    private String password;
    private static final String RECIPIENT_EMAIL = "khanhtqhe163250@fpt.edu.vn";

    private final Properties prop;

    public MailUtils() {
        prop = new Properties();
        prop.put("mail.smtp.auth", true);
        prop.put("mail.smtp.starttls.enable", "true");
        PropertiesFile.setPropertiesFile();
        prop.put("mail.smtp.host", PropertiesFile.getPropValue("mailing.host"));
        prop.put("mail.smtp.port", PropertiesFile.getPropValue("mailing.port"));
        prop.put("mail.smtp.ssl.trust", PropertiesFile.getPropValue("mailing.host"));

        this.username = PropertiesFile.getPropValue("mailing.username");
        this.password = PropertiesFile.getPropValue("mailing.password");
    }

    public static void main(String... args) {
        try {
            MailUtils mailUtils = new MailUtils();
            //Format date
            Date today = new Date();
            SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            String formattedDate = formatter.format(today);

            //Report content in String
            File file = new File("ExtentReports/ExtentReport.html");

            mailUtils.sendMail(
                            RECIPIENT_EMAIL,
                            "MelodyVerse Automation test report " + formattedDate,
                            file);
        } catch (Exception e) {
//            throw new RuntimeException("Can not send email! Please try again later!");
            throw new RuntimeException(e);
        }
    }

    public void sendMail(String recipientEmail, String subject, File content) throws Exception {
        Session session = Session.getInstance(prop, new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        });

        Message message = new MimeMessage(session);
        message.setFrom(new InternetAddress(this.username));
        message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(recipientEmail));
        message.setSubject(subject);

        //create text MimeBodyPart
        MimeBodyPart textPart = new MimeBodyPart();
        String textContent = this.getContentFromFile("src/main/java/utilities/mailContent.txt");
        textPart.setContent(textContent, "text/html; charset=utf-8");;

        //create html file MimeBodyPart
        MimeBodyPart htmlFilePart = new MimeBodyPart();
        htmlFilePart.attachFile(content);

        //MimeBodyPart -> Multipart
        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(textPart);
        multipart.addBodyPart(htmlFilePart);

        //Multipart -> Message
        message.setContent(multipart);

        Transport.send(message);
    }

    public String getContentFromFile(String path){
        byte[] bytes = new byte[0];
        try {
            bytes = Files.readAllBytes(Paths.get(path));
        } catch (IOException e) {
            throw new RuntimeException("Can not read file!");
        }
        return new String(bytes);
    }
}
